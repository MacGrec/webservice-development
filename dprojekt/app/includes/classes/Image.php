<?php

class Image {  
    
    public static function Resize($image_content, $width, $height, $type){

        $origImage = imagecreatefromstring($image_content);
        
        if ($width*$height == 0) {
            if ($width == 0) {
                $width = imagesx($origImage) * $height / imagesy($origImage);
            } elseif ($height == 0) {
                $height = imagesy($origImage) * $width / imagesx($origImage);
            } else {
                return null;
            }
        }
        

        //---Crear la imagen dependiendo de la propiedad a variar
        $image = imagecreatetruecolor($width, $height);
        
        // Make a new transparent image and turn off alpha blending to keep the alpha channel
        imagealphablending($image, false);
        imagesavealpha($image,true);
        $background = imagecolorallocatealpha($image, 255, 255, 255, 127);
        imagecolortransparent($image, $background);
        
        //---Hacer una copia de la imagen dependiendo de la propiedad a variar
        imagecopyresampled($image, $origImage, 0, 0, 0, 0, $width, $height, imagesx($origImage), imagesy($origImage));
 
        //--- Devolver imagen redimensionada
        ob_start();
        switch ($type) {
            case 'image/jpeg':
            case 'image/jpg':
                imagejpeg($image);
                break;
            case 'image/gif':
                imagegif($image);
                break;
            case 'image/png':
                imagepng($image);
                break;
        }
        $imageString = ob_get_contents(); // read from buffer
        ob_end_clean(); // delete buffer
        return $imageString;
    }
    
    public static function ResizeWI($image_content, $width, $height, $type){

        $origImage = imagecreatefromstring($image_content);

        
        // First we try adjusting widht
        $newWidth = $width;
        $newHeight = intval(imagesy($origImage) * $width / imagesx($origImage));
        error_log('New Size: '.$newWidth.'x'.$newHeight);
        
        if ($newHeight > $height) {
            $newHeight = $height;
            $newWidth = intval(imagesx($origImage) * $height / imagesy($origImage));
        }
        

        //---Crear la imagen dependiendo de la propiedad a variar
        $image = imagecreatetruecolor($width, $height);
        
        // Make a new transparent image and turn off alpha blending to keep the alpha channel
        imagealphablending($image, false);
        imagesavealpha($image,true);
        $background = imagecolorallocatealpha($image, 255, 255, 255, 127);
        imagecolortransparent($image, $background);
        
        //---Hacer una copia de la imagen dependiendo de la propiedad a variar
        imagecopyresampled($image, $origImage, intval(($width-$newWidth)/2), intval(($height-$newHeight)/2), 0, 0, $newWidth, $newHeight, imagesx($origImage), imagesy($origImage));
//        imagecopyresampled($image, $origImage, 0, 0, 0, 0, $width, $height, $newWidth, $newHeight);
        //--- Devolver imagen redimensionada
        ob_start();
        switch ($type) {
            case 'image/jpeg':
            case 'image/jpg':
                imagejpeg($image);
                break;
            case 'image/gif':
                imagegif($image);
                break;
            case 'image/png':
                imagepng($image);
                break;
        }
        $imageString = ob_get_contents(); // read from buffer
        ob_end_clean(); // delete buffer
        return $imageString;
    }
    
    public static function Crop($image_content, $cwidth, $cheight, $type, $pos = 'center'){
        
          
        //---Crear la imagen dependiendo de la propiedad a variar
        $image = imagecreatetruecolor($cwidth, $cheight);
        $imageOrig = imagecreatefromstring($image_content);
        

        // Make a new transparent image and turn off alpha blending to keep the alpha channel
        imagealphablending($image, false);
        imagesavealpha($image,true);
        $background = imagecolorallocatealpha($image, 255, 255, 255, 127);
        imagecolortransparent($image, $background);
        
        switch ($pos) {
            case 'center':
                imagecopyresampled($image, imagecreatefromstring($image_content), 0, 0, abs((imagesx($imageOrig) - $cwidth) / 2), abs((imagesy($imageOrig) - $cheight) / 2), $cwidth, $cheight, $cwidth, $cheight);
                break;
            case 'left':
                imagecopyresampled($image, imagecreatefromstring($image_content), 0, 0, 0, abs((imagesy($imageOrig) - $cheight) / 2), $cwidth, $cheight, $cwidth, $cheight);
                break;
            case 'right':
                imagecopyresampled($image, imagecreatefromstring($image_content), 0, 0, imagesx($imageOrig) - $cwidth, abs((imagesy($imageOrig) - $cheight) / 2), $cwidth, $cheight, $cwidth, $cheight);
                break;
            case 'top':
                imagecopyresampled($image, imagecreatefromstring($image_content), 0, 0, abs((imagesx($imageOrig) - $cwidth) / 2), 0, $cwidth, $cheight, $cwidth, $cheight);
                break;
            case 'bottom':
                imagecopyresampled($image, imagecreatefromstring($image_content), 0, 0, abs((imagesx($imageOrig) - $cwidth) / 2), imagesy($imageOrig) - $cheight, $cwidth, $cheight, $cwidth, $cheight);
                break;
        }

             //--- Devolver imagen redimensionada
        ob_start();
        switch ($type) {
            case 'image/jpeg':
            case 'image/jpg':
                imagejpeg($image);
                break;
            case 'image/gif':
                imagegif($image);
                break;
            case 'image/png':
                imagepng($image);
                break;
        }
        $imageString = ob_get_contents(); // read from buffer
        ob_end_clean(); // delete buffer
        return $imageString;
    }   
  
}
?>