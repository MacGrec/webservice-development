<?php 

class Functions {

    public static function complexDeleteDecisions($users_id, $decisions_id = array()){
    	Log::info('complexDeleteDecisions:');
    	
    	if(isset($users_id) && !empty($users_id)){
		 	$decision_types = Config::get('decision_types');
        	$decision_states = Config::get('decision_states');

        	if(!isset($decisions_id) || empty($decisions_id)){
    			//get the list of id's where both are admin's
    			$decisions_id = Functions::getDecisionsIDfromAdmin($users_id);
    		}
    		Log::info('Users deleting  '.var_export( $users_id, true));
    		Log::info('Decisions deleting'.var_export( $decisions_id, true));
        	foreach ($users_id as $user_id) {
	    		foreach ($decisions_id as $decision_id){	    			
    				$decision = Decision::find($decision_id);
    				$decision_user_id = intval($decision->user_id);

    			  
			        if(isset($decision) && $user_id != $decision_user_id && isset($decision_states["CLOSED"]) 
			        	&& $decision->dec_stat_id != $decision_states["CLOSED"] ){
			            //user deleted from participants
			            $participant = new Participant;
			            $participant->user_id = $user_id;
			            $participant->decision_id = $decision_id;
			            $participant->delete();
			            $decision->pushDecision();
			            Log::info('User delete  '.var_export( $participant, true));
			        }
			        elseif (isset($decision_states["CLOSED"]) && $decision->dec_stat_id === $decision_states["CLOSED"]
			         && isset($decision) && $user_id != $decision_user_id ) {
		                //participant marked as exited
		                $participant = Participant::findParticipantByUserID_AND_DecID($user_id, $decision_id);
		            	Log::info('before isset participant 1  '.count($participant));
		                if(isset($participant)){
		                    $participant->part_exited = 1;
		                    $participant->updateParticipant();
		                    $decision->pushDecision();
		                    Log::info('Participant marked as exited 1  '.var_export( $participant, true));
		                }
		            }
	                elseif(sizeof($users_id) === 1 && isset($decision_types["GROUP"]) && $decision->dec_stat_id === $decision_types["GROUP"]){
		                //participant marked as exited
		                $participant = Participant::findParticipantByUserID_AND_DecID($user_id, $decision_id);
		                Log::info('Participant marked as exited 2 '.var_export( $participant, true));
		                if(isset($participant)){
		                    $participant->part_exited = 1;
		                    $participant->updateParticipant();
		                    $decision->pushDecision();
		                    Log::info('Participant marked as exited 2  '.var_export( $participant, true));
		                }
		            }			        
			        elseif (sizeof($users_id) === 1 && isset($decision) && $user_id === $decision_user_id) {
			            
			            //decision marked as deleted (soft deleting)
			            $decision->delete();
			            Log::info('Soft delete '.var_export( $decision, true));
			        }
        		
        		}//end foreach decisions
        	}//end foreach users
    	}//end if    	
    }//end function


 	public static function getDecisionsIDfromAdmin($users_id){
     	$decisions_id = array();
     	foreach ($users_id as $user_id) {
     		$decisions = Decision::findDecisionsByAdmin($user_id);
	     	foreach ($decisions as $decision) {
	     		if(isset($decision->decision_id)){
	     			$decision_id[] = $decision->decision_id;
	     		}
	     	}
     	}     	
     	return $decisions_id;
     }

 	public static function generatePass($size = 6){
     	$characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890_@#$%&*"; //posibles caracteres a usar
		//$size = 6; //numero de letras para generar el texto
		$string = ""; //variable para almacenar la cadena generada
		for($i = 0; $i < $size; $i++)
		{
			$string .= substr($characters,rand(0,strlen($characters)),1); /*Extraemos 1 caracter de los caracteres 
		entre el rango 0 a Numero de letras que tiene la cadena */
		}
		return $string;
     }

 	public static function saveAnalitics($request = NULL, $response = NULL, $error_code = NULL, $error_message = NULL){     	
     	$request = isset($request)? $request: Request::instance();
	    $request_content = $request->getContent();
	    $request_array = json_decode($request_content);
	    $method = Request::method();
	    $function = Request::segment(1);
	    $url = Request::fullUrl();
	    $analitic = new Analitic;
	    
	    if($method === "POST"){
	    	$analitic->user_id = isset($request_array->user_id)? $request_array->user_id : NULL;
	    	$info = isset($request_array->info)? $request_array->info : NULL;
		    if(isset($info)){
		    	$analitic->platform = isset($info->platform)? $info->platform : NULL;
		    	$analitic->operating_system = isset($info->operating_system)? $info->operating_system : NULL;
		    	$analitic->os_version = isset($info->os_version)? $info->os_version : NULL;
		    	$analitic->app_version = isset($info->app_version)? $info->app_version : NULL;
		    }
		    $analitic->request = $request_content;
	    }
	    elseif ($method === "GET"){
	    	$analitic->user_id = Request::input("user_id");
	    	$analitic->platform = Request::input("platform");
	    	$analitic->operating_system = Request::input("operating_system");
	    	$analitic->os_version = Request::input("os_version");
	    	$analitic->app_version = Request::input("app_version");
	    	$analitic->request = $url;
	    }	    

	    $analitic->method = $method;
	    $analitic->function = $function;
	    
	    
	    $analitic->full_request = $request;
	    $error = false;

	    if(isset($response)){
	    	$response_content = $response->getContent();
	    	$analitic->response = $response_content;
	    	$response_array = json_decode($response_content);
	    	if(isset($response_array->error_code)){
	    		$analitic->error_code =$response_array->error_code;
	    		$analitic->error_message = isset($response_array->error_message)? $response_array->error_message : NULL;
	    		$error = true;
	    	}	    	
	    	
	    }
	    elseif (isset($error_code) && isset($error_message)) {
	    	$analitic->error_code = $error_code;
	    	$analitic->error_message = $error_message;
	    	$error = true;
	    }
	    $analitic->error = $error;
	    $analitic->ip = Request::ip();   
	    $analitic->save();
    	
     }

 	public static function diggerFriendly($user_id){
		 $digger_team_id = 1;
		 $array_contacts_role = Config::get('contacts_role');
		 $contact = Contact::findContact($digger_team_id, $user_id);
		 if(!isset($contact) || $contact === false || empty($contact)){
     		$new_contact = new Contact();
	        $secondary_contact = new Contact();

	        $new_contact->contact_primary_user_id = $digger_team_id;
	        $new_contact->contact_secondary_user_id = $user_id;
	        $new_contact->con_ro_id = $array_contacts_role['IS_CONTACT_ROLE']; 
			
	        $new_contact->save();
     	}     	
	 }
 	
     public static function GetinfoDecision($decision, $updated_at, $part_id = NULL, $public = true, $admin = false){
     	Log::info('Decision Acaba de entrar en function: '.var_export( $decision, true));
     	$array_errors = Config::get('ws_errors');
     	$decision_id = $decision->decision_id;
     	$participants_object = Decision::getParticipants($decision_id);
        $participants = array();
        $participants_id = array();     
        $participant_id = NULL;   
        foreach ($participants_object as $participant){

	        $part_user_id = $participant->user_id;
	        $participant_id = $participant->part_id;

	        $preferences = DB::select( DB::raw("SELECT preferences.part_id, preferences.option_id, preferences.updated_at, 
	            preferences.preference_value, options.option_title, options.decision_id,
                participants.user_id
	            FROM preferences LEFT JOIN options
	            ON preferences.option_id = options.option_id
                LEFT JOIN participants ON preferences.part_id = participants.part_id                
                WHERE options.decision_id = $decision_id  AND preferences.part_id = $participant_id
                 ORDER BY preferences.updated_at DESC"));

	         Log::info('particpant_id  '.var_export($participant_id, true));
	         Log::info('user_id  '.var_export($part_user_id, true));
	         Log::info('preferences  '.var_export($preferences, true));
	        //$preferences = Preference::findPreferencesByParticipant($participant->user_id);
	        $preferences_return = array();
	        $pref_update_time = NULL;

	        if(isset($preferences) && !empty($preferences)){                    
	            foreach ($preferences as $preference) {
	                $preference_value = $preference->preference_value;
	                $option_id = $preference->option_id;
	                $array_preference = array("option_id"=>$option_id);
	                if(isset($preference_value))
	                    $array_preference["option_preference"] = $preference_value;
	                $preferences_return[] = $array_preference;
	            }
	            $pref_update_time = isset($preferences[0]) ? $preferences[0]->updated_at : NULL; //in order DESC
	        }

	        $lastname = isset($participant->user_last_nm)? $participant->user_last_nm: " ";

	        $name = isset($participant->user_nm)? $participant->user_nm: $participant->part_nm;
	        
	        $participant_return = array(
	            "id"=>$participant_id,
	            "name"=>$name,
	            "exited"=>$participant->part_exited,
	            "status"=>$participant->par_stat_id
	            );

	        $participant_return["lastname"] = $lastname;

	        if(isset($part_user_id) && !empty($part_user_id)){
	            $participant_return["user_id"] = $part_user_id;
	        } 

	        if(!empty($preferences_return)){
	            $participant_return["preferences"] = $preferences_return;
	        }  

	        if(isset($pref_update_time)){
	            $pref_update_time_timestamp = isset($pref_update_time) ? strtotime($pref_update_time) : null;

	            $participant_return["pref_update_time"] = $pref_update_time_timestamp;
	        }

	        $mult_id = isset($participant->mult_id) ? $participant->mult_id: NULL;
	        if(isset($mult_id)){
	            $multimedia = Multimedia::find($mult_id);
	            $date_array = $multimedia->updated_at;
	            $participant_return["img_update_time"] = $date_array->timestamp;
	        }

	        $participants[] = $participant_return;
	        $participants_id[] = $participant->part_id;

        }//foreach participant


        $options = Option::whereRaw ('decision_id = '.$decision_id)->get(array("option_id","option_title","mult_id"));
        
        $ddl_timestamp = isset($decision->decision_ddl) ? strtotime($decision->decision_ddl) : null;

		$array_options = array();
        foreach ($options as $option) {
            $mult_id = $option->mult_id;
            if(isset($mult_id)){
                $multimedia = Multimedia::find($mult_id);
                $date_array = $multimedia->updated_at;
                $option["img_update_time"] = $date_array->timestamp;
            }
            unset($option["mult_id"]);
            $array_options[] = $option;                
        }
	                    
	     Log::info('Decision antes de crear el response: '.var_export( $decision, true));

	   
	     $return_decision = array(
        'success' => true,
        'decision_id'=>$decision->decision_id, 
        'user_id_admin'=>$decision->user_id, //getDecision  for app version from 1 until 5
        'admin_id' =>$decision->user_id, //getDecision and getDesionList for app version from 6 until..
        'title' => $decision->decision_title,        
        'participants'=>$participants,
        'options'=>$array_options,
        'type'=>$decision->dec_type_id,
        'state'=>$decision->dec_stat_id,
        'update_time'=>$updated_at);            

	    $current_participant = NULL;
     	if (in_array($part_id, $participants_id)){		     
     		$current_participant = $part_id;
     	}	

        if (isset($current_participant) && !empty($current_participant)) {
            $return_decision['participant_id'] = $current_participant;
        }

        $description = $decision->decision_desc;
        if (isset($description) && !empty($description)) {
            $return_decision['description'] = $description;
        }

        $ddl = $ddl_timestamp;
        if (isset($ddl) && !empty($ddl)) {
            $return_decision['ddl'] = $ddl;
        }

        $mult_id = $decision->mult_id;
        if(isset($mult_id)){
            $multimedia = Multimedia::find($mult_id);
            $date_array = $multimedia->updated_at;
            $return_decision["img_update_time"] = $date_array->timestamp;
        }

        $dec_seed = $decision->dec_seed;
        if (isset($dec_seed) && !empty($dec_seed)) {
            $return_decision['decision_seed'] = $dec_seed;
        }

        return $return_decision;
    }     

}
?>