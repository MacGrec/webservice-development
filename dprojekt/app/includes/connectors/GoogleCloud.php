<?php

class GoogleCloud{

	public static function send($array_reg_id, $data, $retry_time = 0){
		Log::info('Send de GoogleCloud ');

		
		$fields = array
		(
			"registration_ids"=>$array_reg_id,	
			 "data"=> $data,
		);

		Log::info('request GCM: '.json_encode($fields));

		$array_gcm = Config::get('google_api_config');

		$url = $array_gcm["gcm_server"];
		$max_time = $array_gcm["max_time"];
		if($retry_time > $max_time)
			return false;
		$api_key = $array_gcm["api_key"];
		$ch = curl_init($url);

		curl_setopt_array($ch, array(
		CURLOPT_POST => TRUE,
		CURLOPT_RETURNTRANSFER => TRUE,
		CURLOPT_HTTPHEADER => array("Content-Type: application/json","Authorization: key=".$api_key),
		CURLOPT_POSTFIELDS => json_encode($fields),
		CURLINFO_HEADER_OUT => TRUE,
		CURLOPT_SSL_VERIFYHOST => FALSE,
		CURLINFO_CONTENT_TYPE 
		));

		curl_setopt($ch, CURLOPT_HEADER,         true);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);	
		//curl_setopt($ch, CURLINFO_HEADER_OUT, true);
		curl_setopt($ch, CURLOPT_VERBOSE, true);


		$response = curl_exec($ch);
		$info = curl_getinfo($ch);
		$error = curl_error($ch);

		$resultHttpCode = isset($info['http_code'])? $info['http_code']: NULL;
		$retry_after = GoogleCloud::getRetryAfter($response);
		$retry_time = (($retry_time * 2) != 0 )? ($retry_time * 2): 2;
    	if(isset($retry_after) && isset($retry_time) && $retry_after >= $retry_time)
    		$retry_time = $retry_after;

    	$response_json = GoogleCloud::getContentGCM($response);
    	Log::info('response GCM: '.$response_json);

    	if(!isset($response_json))
    		return false;
		$response_array = json_decode($response_json);
		switch ($resultHttpCode) {
            case 200:
                //All fine. Continue response processing.
           
            if(!isset($response_array->results)){
            	//TODO save in analitics
            	return false;            	
            }
            $results =  $response_array->results;
            Log::info('Results: '.var_export( $results, true));
            foreach ($results as $key => $value) {
            	$gcm_reg_id = $array_reg_id[$key];
            	//error with some id
            	if(isset($value->error)){            		
            		$error_string = $value->error;
            		Log::info('Hay error: '. $gcm_reg_id. "tipo-> ".$error_string);
            		if ($error_string === "Unavailable") {            			           	
		            	sleep($retry_time);
		            	$response = Gcmregister::send($array_reg_id, $data, $retry_time);
            		}
            		else{
            			Log::info('Antes de borrar: '.$gcm_reg_id);
            			Gcmregister::DeleteByRegId($gcm_reg_id);
            		}
            		
            	}
            	else{//only for debug
            			Log::info('No hay error: '. $gcm_reg_id);
            	}
            	//canonical_ids
            	if(isset($value->registration_id)){
            		$old_gcm_reg_id = $gcm_reg_id;
            		$new_gcm_reg_id = $value->registration_id;
            		$exist = Gcmregister::ExistByRegId($old_gcm_reg_id);
            		if(!$exist){
            			Gcmregister::updateGCM($old_gcm_reg_id, $new_gcm_reg_id);
            		}
            		else{
            			Gcmregister::DeleteByRegId($old_gcm_reg_id);
            		}
            		
            	}
            }	            
                break;
            case 400:
                //throw new Exception('Malformed request. '.$resultBody, Exception::MALFORMED_REQUEST);
            	$content = "Malformed request. 
            	 Full response: 
            	 ". $response;
            	$mail = "team@diggerapp.com";
            	$subject = "[ALERT][GCM][400] Malformed request";
				mail($mail, $subject, $content);
                break;
            case 401:
                //throw new Exception('Authentication Error. '.$resultBody, Exception::AUTHENTICATION_ERROR);
            	$content = "Authentication Error. 
            	Full response:
            	". $response;
            	$mail = "team@diggerapp.com";
            	$subject = "[ALERT][GCM][401] Authentication Error";
				mail($mail, $subject, $content);
                break;
            default:            	          	
            	sleep($retry_time);
            	$response = Gcmregister::send($array_reg_id, $data, $retry_time);
                //throw new Exception("Unknown error. ".$resultBody, Exception::UNKNOWN_ERROR);
                break;
        }

		return true;
	}

	public static function getRetryAfter($response){
		$response_split = preg_split("/\n/", $response);
		$response_grep = preg_grep("/(Retry-After: ([0-9])+)/", $response_split);		
		$response_grep = preg_grep("/: ([0-9])+/", $response_grep);
		$retry_array = explode(": ",(implode(" ",$response_grep)));
		$retry_after = isset($retry_array[1])? $retry_array[1]: NULL;
		return $retry_after;
	}

	public static function getContentGCM($response){
		$pattern = '{"multicast_id":';
		$response_split = preg_split('/'.$pattern.'/', $response);
		$response_json = $pattern;
		$response_json .= isset($response_split[1])? $response_split[1]: NULL;
		return $response_json;
	}

	public static function save($user_id, $gcm_reg_id = NULL){

        if(isset($gcm_reg_id) && !empty($gcm_reg_id)){
            $gcm_register = Gcmregister::where(array('gcm_reg_id'=> $gcm_reg_id))->first();

            if(!isset($gcm_register) || empty($gcm_register) || $gcm_register === false){
                $gcm_register = new Gcmregister;
                $gcm_register->user_id = $user_id;
                $gcm_register->gcm_reg_id = $gcm_reg_id;
                $gcm_register->save();            
            }
            else{
                //if exist the gcm_reg_id
                if($gcm_register->user_id != $user_id){
                    Gcmregister::updateUserIDByGCM_regID($user_id, $gcm_reg_id);
                }           
            }


            return true;
        }

        return false;
		
	}
}
?>