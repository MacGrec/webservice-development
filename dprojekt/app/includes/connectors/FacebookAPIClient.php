<?php
class FacebookAPIClient{
	public static function getDataFromToken($token) {
		$array_google_api = Config::get('facebook_api_config');

		$app_id = $array_google_api["app_id"];
		$app_secret = $array_google_api["app_secret"];
		$api_version = $array_google_api["api_version"];

		$fb = new Facebook\Facebook([
		'app_id' => $app_id,
		'app_secret' => $app_secret,
		'default_graph_version' => $api_version,
  		]);


  		$fb->setDefaultAccessToken($token);
  		$error = NULL;
		try {
		  $response = $fb->get('/me?fields=id,name,email');
		  $graphNode = $response->getGraphNode();
		} catch(Facebook\Exceptions\FacebookResponseException $e) {
		  // When Graph returns an error
		  $error = 'Graph returned an error: ' . $e->getMessage();
		  exit;
		} catch(Facebook\Exceptions\FacebookSDKException $e) {
		  // When validation fails or other local issues
		  $error = 'Facebook SDK returned an error: ' . $e->getMessage();
		  exit;
		}


		$arrayFields = $graphNode->getFieldNames();

		$email = in_array('email', $arrayFields)? $graphNode->getField('email'): NULL;

		/*$name = $userNode->getName();

		$firstName = $userNode->getFirstName();*/

		$info = array(
			"email" => $email,

			);

		return $info;
	}
}

?>