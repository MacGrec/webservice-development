<?php
class GoogleAPIClient{
	public static function getDataFromToken($token) {
		$array_google_api = Config::get('google_api_config');

		$api_key = $array_google_api["api_key"];
		$client_secret = $array_google_api["client_secret"];
		$server_client_id = $array_google_api["server_client_id"];

		$client = new Google_Client();
		$client->setClientId($server_client_id);
		$client->setClientSecret($client_secret);
		$client->setDeveloperKey($api_key);

		try {
			$token_data = $client->verifyIdToken($token)->getAttributes();
		} catch (exception $e) {
			$error_message = $e->getMessage();
			return array("error_message" => $error_message);
		}

		//Verify Token Fields
		$info = GoogleAPIClient::verifyTokenFields($token_data);
		return $info;
	}

	public static function verifyTokenFields($token_data){
		$array_google_api = Config::get('google_api_config');

		$server_client_id = $array_google_api["server_client_id"];
		$app_client_id = $array_google_api["app_client_id"];

		if(isset($token_data['payload'])){
			if(isset($token_data['payload']['aud'])){
				if($token_data['payload']['aud'] != $server_client_id){
					$error_message = 'server client ID not match: '. $server_client_id;
				}
				if(isset($token_data['payload']['azp'])){
					if($token_data['payload']['azp'] != $app_client_id){
						$error_message = 'app client ID not match: '. $app_client_id ;
					}
					if(isset($token_data['payload']['email'])){
						$email = $token_data['payload']['email'];
						return array("email" => $email);
					}
					else{
						$error_message = "email not exist";
					}
				}
				else{
					$error_message = "app client ID not exist";
				}
			}
			else{
				$error_message = "server client ID not exist";
			}	
		}
		else{
			$error_message = "payload not exist";
		}

		return array("error_message" => $error_message);
	}
}



?>