<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>{{ $name}}: Reset password</h2>

		<div>
			Obtain a new password:
		</div>
		<div>Click <a href={{ $detail }} style="font-weight:bold" target= "_blank" title="Reset email">here</a></div>
	</body>
</html>