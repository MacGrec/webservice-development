<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>{{$name}}: New password</h2>

		<div>
			This is your new password:
		</div>
		<div>Your new password is {{$pass}}</div>
	</body>
</html>
