<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>Feedback {{ $type }}:</h2>
		<br>
		<div>El usuario {{ $user}} con email {{$user_email}} ha enviado el siguiente feedback:<br><br></div>
		<div>{{ $text }}</div>
	</body>
</html>