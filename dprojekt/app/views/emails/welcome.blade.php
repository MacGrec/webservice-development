<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>{{ $name}}: Welcome to D-projekt</h2>

		<div>
			Please confirm your account:
		</div>
		<div>Click <a href={{ $detail }} style="font-weight:bold" target= "_blank" title="Verify Account">here</a></div>
	</body>
</html>