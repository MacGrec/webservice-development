

<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  


<script>
$(function () {
           
     $("#from").datepicker({
        changeYear: true,
        changeMonth: true,
        dayNamesMin: [  "Dom","Lun", "Mar", "Mie", "Jue", "Vie", "Sáb"],
        firstDay: 1,
        monthNamesShort: [ "Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic" ],
        monthNames: [ "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" ],
        dateFormat: "yy-mm-dd",
        onClose: function (selectedDate) {
            $("#to").datepicker("option", "minDate", selectedDate);
        }
    });
    $("#to").datepicker({
        changeYear: true,
        changeMonth: true,
        dayNamesMin: ["Dom","Lun", "Mar", "Mie", "Jue", "Vie", "Sáb"],
        firstDay: 1,
        monthNamesShort: [ "Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic" ],
        monthNames: [ "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" ],
        dateFormat: "yy-mm-dd",
        onClose: function (selectedDate) {
            $("#from").datepicker("option", "maxDate", selectedDate);
        }
    });
        
});
</script>    

<!--Load the AJAX API-->
<script type="text/javascript" src="https://www.google.com/jsapi">  </script>

<script type="text/javascript">
//$('#chart_div').hide();
  
    // Load the Visualization API library and the piechart library.
    google.load('visualization', '1.0', {'packages':['corechart']});
  
     // ... draw the chart...
     google.setOnLoadCallback(drawChart);
     function callDraw() {        
         
     }

     function drawChart(title, type) {
        $('#chart_div').show();
        title = title ||'Decisiones creadas';
        title = (typeof title == 'object') || (typeof title== 'undefined')? "Decisiones creadas" : title;
        type = (typeof type == 'object') || (typeof type == 'undefined')? "Line" : type;
       
      // Create the data table.
      var data = new google.visualization.DataTable();
      data.addColumn('string', 'Meses');
      data.addColumn('number', 'Decisiones');
      data.addRows([
        ['Enero', 3],
        ['Febrero', 1],
        ['Marzo', 1], 
        ['Abril', 1],
        ['Mayo', 2]
      ]);
      // Set chart options
      var options = {'title':title,
                     'width':600,
                     'height':500};

      // Instantiate and draw our chart, passing in some options.
      switch(type) {
    case 'Line':
        var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
        break;
    case 'Pie':
        var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
        break;
    default:
        break;
    }
      if (typeof chart != 'undefined') {
          // ..
          chart.draw(data, options);
        }
      
    }
</script>

<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Ver</title>
	<style>
		
		body {
			margin:0;
			font-family:'Lato', sans-serif;
			text-align:center;
			color: #999;
		}

		.Generatd {
			width: 300px;
			height: 200px;
			position: absolute;
			left: 50%;
			top: 50%;
			margin-left: -150px;
			margin-top: -100px;
		}
        #chart_div{
            width: 500px;
            height: 400px;
            position: absolute;
            margin-left: 450px;
        }

		a, a:visited {
			text-decoration:none;
		}

		h1 {
			font-size: 32px;
			margin: 16px 0 0 0;
		}
	</style>
</head>
<body>
	<div class="header">
	<h1> Estadisticas de uso de Digger <h1>
	<!--<h1><?php echo $info; ?></h1>-->
	</div>
	<form method='post' name='stats'>
    <div style="position: relative; width: 100%; margin-bottom: 8px;">
        <div style="position: absolute; width: 155px;"><label for="listStat">Estadisticas:</label></div>
        <div>
            <select name="stat_type">
                <option value="createDec">Deciones Creadas</option><option value="active_users">Usuarios Activos</option>
            </select>
        </div>
    </div>  
    
    <div style="position: relative; width: 100%; margin-bottom: 8px;">
        <div style="position: absolute; width: 155px;"><label for="interval">Intervalo:</label></div>
        <div>
            <p>
                Desde:
                <input class="date" type="text" id="from" name="from" />
                Hasta:
                <input class="date" type="text" id="to" name="to" />
            </p>
        </div>
    </div> 
     <p><input class='form_submit' type='submit' value='Buscar'>

</form>
	<?php 
	$decisions = Decision::all();
 
    var_dump($_POST);
    $stat_type = isset($_POST['stat_type'])? $_POST['stat_type'] : NULL;
    $from = isset($_POST['from'])? $_POST['from'] : NULL;
    $to = isset($_POST['to'])? $_POST['to'] : NULL;
    var_dump($from);

    $decisions =  DB::select( DB::raw("SELECT decisions.created_at
                FROM decisions 
                WHERE 
                    decisions.created_at > '$from' 
                    AND decisions.created_at < '$to' 
                 ORDER BY decisions.created_at 
                 DESC")); 

	//var_dump($decisions);
    $title = NULL;
    switch ($stat_type) {
        case 'createDec':
            $title = "Decisiones creadas";
            break;
        case 'active_users':
            $title = "Usuarios activos";
            break;        
        default:
            # code...
            break;
    }
    $type = array("line"=>"Line", "pie"=>"Pie");
    foreach ($decisions as $decision) {
        $timestamp = strtotime($decision->created_at);
        $array_time = getdate($timestamp);
        var_dump($array_time);
    }
	?>
   
<input style="cursor:pointer;margin-left: 5px;" type="button" onclick="callDraw()" value="test">
  
<input style="cursor:pointer;margin-left: 5px;" type="button" onclick="drawChart('<?php echo $title?>','<?php echo $type['line']?>')" value="Grafica de Linea">
    <input style="cursor:pointer;margin-left: 5px;" type="button" onclick="drawChart('<?php echo $title?>','<?php echo $type['pie']?>')" value="Grafica de pastel">
    <div id="chart_div" style="width:400; height:300"> </div>
</body>
</html>
