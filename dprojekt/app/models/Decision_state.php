<?php


class Decision_state extends Eloquent {
	protected $primaryKey = 'dec_stat_id';

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'decision_states';
	protected $fillable = array('dec_stat_nm');
	public function decision(){
		return $this->belongsTo('Decision_state', 'dec_stat_id');
	}
}
?>