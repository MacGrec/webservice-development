<?php


class Participant extends Eloquent {
	protected $primaryKey = 'part_id';

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'participants';
	protected $fillable = array('part_id','user_id','decision_id','part_nm','part_email','part_exited','par_stat_id');
	
	use SoftDeletingTrait;

    protected $dates = ['deleted_at'];

	/*public function user(){
		return $this->hasOne('User','user_id');
	}

	public function decisions(){
        return $this->belongsToMany('Decision','decision_id');
    }
*/

	public function updateParticipant(array $params = array())
	{
		$part_exited = isset($this->attributes['part_exited'])?$this->attributes['part_exited']:0;
		$par_stat_id = $this->attributes['par_stat_id'];
		$part_id = $this->attributes['part_id'] ;
		$decision_id = $this->attributes['decision_id'];
		$timestamp = date('Y-m-d H:i:s', time());

    	return DB::statement("UPDATE `participants` 
    		SET `part_exited`  = '$part_exited'
    		, `par_stat_id` = '$par_stat_id'
    		, `updated_at` = '$timestamp'
    		WHERE `participants`.`part_id` = $part_id 
    		AND `participants`.`decision_id`  = $decision_id");
	}

	
	/*public static function findParticipantByUserID($user_id, $decision_id)
	{
		return Participant::where(array('decision_id'=>$decision_id, 'user_id'=>$user_id))
    	 ->first();

	}*/

	public static function findParticipantByUserID_AND_DecID($user_id, $decision_id)
	{
		return Participant::where(array('decision_id'=>$decision_id, 'user_id'=>$user_id))
    	 ->first();

	}

	public function delete(array $params = array())
	{
    	return DB::connection($this->getConnectionName())
        	->table($this->getTable())
        	->where('user_id', $this->attributes['user_id'])
       	 	->where('decision_id', $this->attributes['decision_id'])
        	->delete();
	}
}
?>