<?php 
class Country extends Eloquent {
	protected $primaryKey = 'country_id';
    protected $table = 'countries';
    protected $fillable = array('country_nm','country_ISO3','mult_id');

	public function user(){
			return $this->belongsTo('User', 'user_id');
	}

	public static function findCountryByISO3($country_ISO3)
	{
		return Country::where(array('country_ISO3'=>$country_ISO3))
    	 ->first();

	}
    
}
?>