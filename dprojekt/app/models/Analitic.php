<?php 
class Analitic extends Eloquent {
	protected $primaryKey = 'id';
    protected $table = 'analytics';
    protected $connection = 'analytics';
    protected $fillable = array('user_id','function','method','request','response','platform','operating_system',
    	'os_version','app_version','error','error_code','error_message','ip','full_request');

	
    
}
?>