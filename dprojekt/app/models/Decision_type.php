<?php


class Decision_type extends Eloquent {
	protected $primaryKey = 'dec_type_id';

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'decision_types';
	protected $fillable = array('dec_type_nm');
	public function decision(){
		return $this->belongsTo('Decision_type', 'dec_type_id');
	}
}
?>