<?php


class Contact extends Eloquent {
	protected $primaryKey = array('contact_primary_user_id','contact_secondary_user_id');
	public $incrementing = false;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'contacts';
	protected $fillable = array('contact_primary_user_id','contact_secondary_user_id','con_ro_id');


	/*public function user_Primary(){
		return $this->hasOne('User','user_id');
	}

	public function user_Secondary(){
		return $this->hasOne('User','user_id');
	}*/

	public static function findContact($contact_primary_user_id, $contact_secondary_user_id)
	{
		return Contact::where(array('contact_primary_user_id'=>$contact_primary_user_id, 
			'contact_secondary_user_id'=>$contact_secondary_user_id))
    	 ->first();

	}

	public static function isContact($contact_primary_user_id, $contact_secondary_user_id)
	{
		$contact = Contact::where(array('contact_primary_user_id'=>$contact_primary_user_id, 
			'contact_secondary_user_id'=>$contact_secondary_user_id, 'con_ro_id'=>1))
    	 ->first();

    	 $exist = (count($contact) > 0) ? true: false; 
    	 return $exist;

	}

	public function updateContact(array $params = array())//TODO
	{
		$contact_primary_user_id = $this->attributes['contact_primary_user_id'];
		$contact_secondary_user_id = $this->attributes['contact_secondary_user_id'];
		$con_ro_id = $this->attributes['con_ro_id'];
		$timestamp = date('Y-m-d H:i:s', time());

    	return DB::statement("UPDATE contacts SET contacts.con_ro_id = $con_ro_id
    		, `updated_at` = '$timestamp'
    		WHERE contacts.contact_primary_user_id = $contact_primary_user_id
    		AND contacts.contact_secondary_user_id = $contact_secondary_user_id");
	}

	public static function pushContact($user_id){
		$timestamp = date('Y-m-d H:i:s', time());

    	return DB::statement("UPDATE `contacts` 
    		SET `updated_at` = '$timestamp'
    		WHERE `contacts`.`contact_primary_user_id` = $user_id or `contacts`.`contact_secondary_user_id` = $user_id");
	}

	public function update(array $params = array())
	{
    	return DB::connection($this->getConnectionName())
        	->table($this->getTable())
        	->where('contact_primary_user_id', $this->attributes['contact_primary_user_id'])
       	 	->where('contact_secondary_user_id', $this->attributes['contact_secondary_user_id'])
        	->update($params);
	}
}
?>