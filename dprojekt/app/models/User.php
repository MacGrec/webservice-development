<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class User extends Eloquent {
	protected $primaryKey = 'user_id';

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';
	protected $fillable = array('user_nm','user_last_nm','user_sex','user_city','country_id','mult_id', 'user_birth_date');

	use SoftDeletingTrait;

    protected $dates = ['deleted_at'];

	public static function findUserByEmail($email)
	{
		return User::where(array('user_email'=>$email))
    	 ->first();

	}

	public function pushObject(array $params = array()){
		$user_id = $this->attributes['user_id'];
		$timestamp = time();
		$human_time = date('Y-m-d H:i:s', $timestamp);
		 

    	DB::statement("UPDATE `users` 
    		SET `updated_at` = '$human_time'
    		WHERE `users`.`user_id` = $user_id");
    	return $timestamp;
	}

	public function country(){
		return $this->hasOne('Country','country_id');
	}
	public function multimedia(){
		return $this->hasOne('Multimedia','mult_id');
	}
	public function decision(){
		return $this->belongsTo('Decision', 'decision_id');
	}
	public function option(){
		return $this->belongsTo('Option', 'option_id');
	}
	public function gcm_register(){
		return $this->belongsToMany('Gcm_register', 'reg_id');
	}
}
?>
