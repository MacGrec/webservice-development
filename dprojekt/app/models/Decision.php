<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Decision extends Eloquent {
	protected $primaryKey = 'decision_id';

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'decisions';
	protected $fillable = array('decision_title','decision_desc','mult_id','user_id','dec_stat_id','dec_type_id','dec_seed','dec_admin_seed','decision_ddl'); 

	use SoftDeletingTrait;

    protected $dates = ['deleted_at'];


	public function pushDecision(array $params = array()){
		$decision_id = $this->attributes['decision_id'];
		$timestamp = time();
		$human_time = date('Y-m-d H:i:s', $timestamp);
		 

    	DB::statement("UPDATE `decisions` 
    		SET `updated_at` = '$human_time'
    		WHERE `decisions`.`decision_id` = $decision_id");
    	return $timestamp;
	}

	public function pushObject(array $params = array()){
		$decision_id = $this->attributes['decision_id'];
		$timestamp = time();
		$human_time = date('Y-m-d H:i:s', $timestamp);
		 

    	DB::statement("UPDATE `decisions` 
    		SET `updated_at` = '$human_time'
    		WHERE `decisions`.`decision_id` = $decision_id");
    	return $timestamp;
	}

	public static function findDecisionsByAdmin($user_id)
	{
		return Decision::where(array('user_id'=>$user_id))
    	 ->get();

	}

	public static function findDecisionsBySeed($dec_seed)
	{
		return Decision::where(array('dec_seed'=>$dec_seed))
    	 ->first();

	}

	public static function findDecisionsByAdminSeed($dec_admin_seed)
	{
		return Decision::where(array('dec_admin_seed'=>$dec_admin_seed))
    	 ->first();

	}

	public static function getParticipants($decision_id){
		$participants_object = DB::select( DB::raw("SELECT users.user_id, users.user_nm, users.user_last_nm, users.mult_id,
	 		participants.decision_id, participants.part_id, participants.part_nm ,participants.part_exited, participants.par_stat_id 
 			FROM users 
            RIGHT JOIN participants ON users.user_id = participants.user_id 
            WHERE participants.decision_id = $decision_id    
            GROUP BY participants.part_id"));

        return $participants_object;
	}

	public function multimedia(){
		return $this->hasOne('Multimedia','mult_id');
	}
	public function user(){
		return $this->hasOne('User','user_id');
	}
	public function decision_state(){
		return $this->hasOne('Decision_state','dec_stat_id');
	}
	public function decision_type(){
		return $this->hasOne('Decision_type','dec_type_id');
	}

	public function option(){
		return $this->belongsTo('Option', 'option_id');
	}

	public function participants(){
        return $this->belongsToMany('Participant');
    }

}
?>