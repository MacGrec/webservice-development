<?php


class Option extends Eloquent {
	protected $primaryKey = 'option_id';

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'options';
	protected $fillable = array('decision_id','option_title','option_desc','mult_id','option_rank');
	public function decision(){
		return $this->hasOne('Decision','decision_id');
	}
	public function multimedia(){
		return $this->hasOne('Multimedia','mult_id');
	}

	public function pushObject(array $params = array()){
		$option_id = $this->attributes['option_id'];
		$timestamp = time();
		$human_time = date('Y-m-d H:i:s', $timestamp);
		 

    	DB::statement("UPDATE `options` 
    		SET `updated_at` = '$human_time'
    		WHERE `options`.`option_id` = $option_id");
    	return $timestamp;
	}
}
?>