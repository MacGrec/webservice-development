<?php


class Contact_role extends Eloquent {
	protected $primaryKey = 'con_ro_id';


	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'contact_roles';
	protected $fillable = array('con_ro_type','con_ro_desc'); 
	public function contact(){
		return $this->belongsTo('Contact', 'con_ro_id');
	}

}