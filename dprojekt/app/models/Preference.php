<?php

class Preference extends Eloquent {
	protected $primaryKey = array('part_id','option_id');
	public $incrementing = false;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'preferences';
	protected $fillable = array('part_id','option_id','preference_value');
	
	use SoftDeletingTrait;

    protected $dates = ['deleted_at'];

	public function updatePreference(array $params = array())
	{
		
		$part_id = $this->attributes['part_id'];
		$option_id = $this->attributes['option_id'];
		$preference_value = $this->attributes['preference_value'];
		$now = time();
		$timestamp = date('Y-m-d H:i:s', $now);

		if(!isset($preference_value)){
			DB::statement("UPDATE `preferences` 
    		SET `preference_value`  = NULL
    		, `updated_at` = '$timestamp'
    		WHERE `preferences`.`part_id` = $part_id
    		AND `preferences`.`option_id`  = $option_id");
		}
		else{
			DB::statement("UPDATE `preferences` 
    		SET `preference_value`  = $preference_value
    		, `updated_at` = '$timestamp'
    		WHERE `preferences`.`part_id` = $part_id 
    		AND `preferences`.`option_id`  = $option_id");
		}

		return $now;
    	
	}

	
	public static function findPreference($part_id, $option_id)
	{
		return Preference::where(array('option_id'=>$option_id, 'part_id'=>$part_id))
    	 ->first();

	}

	public static function findPreferencesByParticipant($part_id)
	{
		
		return Preference::where(array('part_id'=>$part_id))
    	 ->get();

	}

	public function delete(array $params = array())
	{
    	return DB::connection($this->getConnectionName())
        	->table($this->getTable())
        	->where('part_id', $this->attributes['part_id'])
       	 	->where('option_id', $this->attributes['option_id'])
        	->delete();
	}
}