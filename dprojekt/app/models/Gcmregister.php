<?php

class Gcmregister extends Eloquent {
	protected $primaryKey = 'user_id';//gcm_reg_id
	public $incrementing = false;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'gcm_registers';
	protected $fillable = array('user_id','gcm_reg_id');


	
	public function user(){
		return $this->hasOne('User','user_id');
	}

	
	/*public function save(array $params = array())
	{
    	return DB::connection($this->getConnectionName())
        	->table($this->getTable())
        	->where('user_id', $this->attributes['user_id'])
       	 	->where('gcm_reg_id', $this->attributes['gcm_reg_id'])
        	->update($params);
	}*/
	public static function getGCMsByRegId($gcm_reg_id){

		return Gcmregister::where('gcm_reg_id', '=', '$gcm_reg_id')->get();

	}

	
	public static function ExistByRegId($gcm_reg_id)
	{
		$gcmObjects = Gcmregister::getGCMsByRegId($gcm_reg_id);
		if(isset($gcmObjects) && $gcmObjects != false)
			return true;
		return false;

	}

	public static function getRegIDsByUserId($user_id){
		$array_regs_id = array();
        $gcm_registers = Gcmregister::where(array('user_id' => $user_id))->get();
        if(isset($gcm_registers) && !empty($gcm_registers) && $gcm_registers !== false){
            foreach ($gcm_registers as $gcm_register) {
                $array_regs_id[] = $gcm_register->gcm_reg_id;  
            }                              
        }
        Log::info('User ID en Gcmregister: '.$user_id);
        Log::info('Tamaño array de registros gcm en Gcmregister: '.count($array_regs_id));
        Log::info('$array_regs_id: '.var_export( $array_regs_id, true));
        return $array_regs_id;
	}
	public static function updateGCM($old_gcm_reg_id, $new_gcm_reg_id){
		$timestamp = date('Y-m-d H:i:s', time());

    	return DB::statement("UPDATE `gcm_registers` 
    		SET gcm_registers.gcm_reg_id = '$new_gcm_reg_id',
    		gcm_registers.updated_at = '$timestamp'
    		WHERE gcm_registers.gcm_reg_id = '$old_gcm_reg_id'");
	}

	public static function updateUserIDByGCM_regID($user_id, $gcm_reg_id){
		$timestamp = date('Y-m-d H:i:s', time());

    	return DB::statement("UPDATE `gcm_registers` 
    		SET gcm_registers.user_id = '$user_id',
    		gcm_registers.updated_at = '$timestamp'
    		WHERE gcm_registers.gcm_reg_id = '$gcm_reg_id'");
	}

	public function delete(array $params = array())
	{
    	return DB::connection($this->getConnectionName())
        	->table($this->getTable())
        	->where('user_id', $this->attributes['user_id'])
       	 	->where('gcm_reg_id', $this->attributes['gcm_reg_id'])
        	->delete();
	}

	public static function DeleteByRegId($gcm_reg_id)
	{
		Log::info('En DeleteByRegId: '.$gcm_reg_id);
		return Gcmregister::where('gcm_reg_id', '=', $gcm_reg_id)->delete();

	}
	public static function DeleteByUserId($user_id)
	{

		return Gcmregister::where('user_id', '=', $user_id)->delete();

	}
}
?>