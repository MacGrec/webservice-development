<?php 
class Feedback extends Eloquent {
	protected $primaryKey = 'id';
    protected $table = 'feedbacks';
    protected $connection = 'analytics';
    protected $fillable = array('user_id','feedback_type','feedback_text','platform','operating_system',
    	'os_version','app_version');

	
    
}
?>