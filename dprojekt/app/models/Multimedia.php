<?php


class Multimedia extends Eloquent {
	protected $primaryKey = 'mult_id';

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'multimedia';
	protected $fillable = array('mult_content','mult_content_type','mult_link');
	

	public function option(){
		return $this->belongsTo('Option', 'option_id');
	}

	public function decision(){
		return $this->belongsTo('Decision', 'decision_id');
	}
	public function user(){
		return $this->belongsTo('User', 'user_id');
	}

	    
}
?>