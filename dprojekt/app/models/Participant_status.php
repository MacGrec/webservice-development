<?php


class Participant_status extends Eloquent {
	protected $primaryKey = array('par_stat_id');
	public $incrementing = false;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'participant_status';
	protected $fillable = array('par_stat_id','par_stat_nm');	

}
?>