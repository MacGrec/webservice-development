<?php
return array(
300 => 'Not enough parameters',

102 => 'The email entered is already registered',

103 => "The email entered doesn't exist",

104 => 'The password entered is incorrect',

105 => 'Account needs to be confirmed',

106 => 'The format of the email is invalid',

666 => 'You are possessed',

107 => 'Incorrect format of parameter',

108 => 'ID not exist',

109 => "Bad deadline",

110 => "Incomplete decision",

000 => 'You know nothing Jon Snow',

111 => "The user is not a participant",

112 => "The user blocked you or you blocked him/her",

113 => "The value enum is not correct",

114 => "The user is not the admin",

115 => "Duplicate Seed",

600 => "Verify Google token failed",

601 => "Not email inside the Google token",

116 => "App out of date",

117 => "The attribute is oversize",

);
?>