<?php
include_once(app_path().'/includes/tools/Functions.php');

/*
|--------------------------------------------------------------------------
| Register The Laravel Class Loader
|--------------------------------------------------------------------------
|
| In addition to using Composer, you may use the Laravel class loader to
| load your controllers and models. This is useful for keeping all of
| your classes in the "global" namespace without Composer updating.
|
*/

ClassLoader::addDirectories(array(

	app_path().'/commands',
	app_path().'/controllers',
	app_path().'/models',
	app_path().'/database/seeds',

));

/*
|--------------------------------------------------------------------------
| Application Error Logger
|--------------------------------------------------------------------------
|
| Here we will configure the error logger setup for the application which
| is built on top of the wonderful Monolog library. By default we will
| build a basic log file setup which creates a single file for logs.
|
*/

Log::useFiles(storage_path().'/logs/laravel.log');

/*
|--------------------------------------------------------------------------
| Application Error Handler
|--------------------------------------------------------------------------
|
| Here you may handle any errors that occur in your application, including
| logging them or displaying custom views for specific errors. You may
| even register several error handlers to handle different types of
| exceptions. If nothing is returned, the default error view is
| shown, which includes a detailed stack trace during debug.
|
*/

App::error(function(Exception $exception, $code)
{
    $error_message = "unhandled error: ".$exception;
    $error_code = 666;
	Log::error($exception);
	 if ($exception instanceof Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException)
        {
            $error_message = "Wrong Method sending data";
            $error_code = 405;    
        	Log::warning($error_message);
            return Response::view('errors.wrong-method', array(), $error_code);
        }
        if($exception instanceof Illuminate\Database\QueryException)
        {
            $error_message = "QueryException: ".$exception;
            $error_code = 409;
        	Log::warning($error_message);
            return Response::view('errors.wrong-query', array(), $error_code);
        }

        Functions::saveAnalitics(NULL, NULL, $error_code, $error_message);        
});

/*
|--------------------------------------------------------------------------
| Maintenance Mode Handler
|--------------------------------------------------------------------------
|
| The "down" Artisan command gives you the ability to put an application
| into maintenance mode. Here, you will define what is displayed back
| to the user if maintenance mode is in effect for the application.
|
*/

App::down(function()
{
    $error_message = "Be right back!";
    $error_code = 503;
    Functions::saveAnalitics(NULL, NULL, $error_code, $error_message);
	return Response::make("Be right back!", $error_code);
});

/*
|--------------------------------------------------------------------------
| Require The Filters File
|--------------------------------------------------------------------------
|
| Next we will load the filters file for the application. This gives us
| a nice separate location to store our route and application filter
| definitions instead of putting them all in the main routes file.
|
*/

require app_path().'/filters.php';


//throw the htttp header 404 if dont found the function in our WS
App::missing(function($e) 
{
    $url = Request::fullUrl();
    $error_code = 404;
    Log::warning($error_code." for URL: $url");

    $error_message = "Function not found";
    Functions::saveAnalitics(NULL, NULL, $error_code, $error_message);
    return Response::view('errors.not-found', array(), $error_code);    
});


