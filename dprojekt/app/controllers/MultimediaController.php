<?php 


class MultimediaController extends BaseController {

    public function testHash(){
        /**
         * Este código evaluará el servidor para determinar el coste permitido.
         * Se establecerá el mayor coste posible sin disminuir demasiando la velocidad
         * del servidor. 8-10 es una buena referencia, y más es bueno si los servidores
         * son suficientemente rápidos. El código que sigue tiene como objetivo un tramo de
         * ≤ 50 milisegundos, que es una buena referencia para sistemas con registros interactivos.
         */
        $timeTarget = 0.05; // 50 milisegundos 

        $coste = 8;
        do {
            $coste++;
            $inicio = microtime(true);
            password_hash("test", PASSWORD_BCRYPT, ["cost" => $coste]);
            $fin = microtime(true);
        } while (($fin - $inicio) < $timeTarget);

        echo "Coste conveniente encontrado: " . $coste . "\n";

    }

	public function getImage(){
        $array_errors = Config::get('ws_errors');
        $array_contacts_role = Config::get('contacts_role'); 

               
        //define parameters validator
        $rules = array(
            'user_id' => 'integer|exists:users,user_id',
            'request_type' => 'required|in:user,decision,option',
            'content_id' => 'required|integer',
            'type' => 'in:c,r,cr,rc|required_with:width,height',
            'width' => 'integer|required_with:type,height',
            'height' => 'integer|required_with:width,type',
        );         
        $messages = array(
            'required' => 'The :attribute field is required.',
            'required_with' => 'The :attribute field is required.',
            'exists' => 'The :attribute not exist.',
            'integer' => 'The :attribute must be a integer',
            'in' => 'The :attribute must be the correct type',
        );
        // doing the validation, passing post data and rules
        $validator = Validator::make(Input::all(), $rules, $messages);  
     
        $array_messages_error = $validator->messages()->getMessages();
        
        $error_message = array();   
        $error_code = 666;
        if ($validator->fails()){
            foreach ($array_messages_error as $key => $value) {
                $error_message[$key] = $value[0];
                $needle = 'field is required';
                $pos = strripos($value[0], $needle);
                //falta algún campo requerido
                if($pos !== false){
                    $error_code = 300;
                    break;
                }
                else{
                    $needle = 'not exist';
                    $pos = strripos($value[0], $needle);
                    if($pos !== false){
                        $error_code = 108;
                        break;
                    }
                    else{
                        $needle = 'must be a integer';
                        $pos = strripos($value[0], $needle);
                        if($pos !== false){
                            $error_code = 107;
                            break;
                        }
                        else{
                            $needle = 'must be the correct type';
                            $pos = strripos($value[0], $needle);
                            if($pos !== false){
                                $error_code = 113;
                                break;
                            }
                        }
                    }
                }  
            }
            return Response::json(array(
                'success' => false,
                'error_code' => $error_code,
                'error_message' => $array_errors[$error_code]),
                200
                );
        }
        //end validation

        //capture the variables
        $user_id = Input::get('user_id', NULL);
        $request_type = Input::get('request_type', NULL);
        $content_id = Input::get('content_id', NULL);

        $type = Input::get('type', NULL);
        $width = Input::get('width', NULL);
        $height = Input::get('height', NULL);

        $image_conten = NULL;
        /*if(isset($type) && isset($width) && isset($height)){            
            $multimedia = Multimedia::find($user->mult_id);
            $mul_content = $multimedia->mult_content;
            $type =  $multimedia->mult_content_type;
            switch ($type) {
                case 'c':
                    $image_content = Image::Crop($mul_content, $width, $height, $type);
                    break;

                case 'r':
                    $image_content = Image::Resize($mul_content, $width, $height, $type);
                    break;

                case 'rc':
                    $image_content = Image::Resize($mul_content, $width, $height, $type);
                    $image_content = Image::Crop($image_content, $width, $height, $type);
                    break;

                case 'cr':
                    $image_content = Image::Crop($mul_content, $width, $height, $type);
                    $image_content = Image::Resize($image_content, $width, $height, $type);
                    break;

                default:
                    break;
            }
        }*/
        $mult_id = NULL;
        $object = NULL;
        switch ($request_type) {            
            case 'user':
                if(isset($user_id)){
                    $blocked_role = $array_contacts_role['I_BLOCKED_ROLE'];      
                    $contact_opos = Contact::findContact($content_id,$user_id);
                    if (!isset($contact_opos) || $contact_opos->con_ro_id != $blocked_role) {
                        $object = User::find($content_id);  
                    } 
                }
                else{
                    $object = User::find($content_id);
                }                                  
                break;
            case 'decision':
                $object = Decision::find($content_id);            
                break;
            case 'option':
                $object = Option::find($content_id);
                break;            
            default:
                break;
        }

        if(!isset($object)){
            $error_code = 108;
            return Response::json(array(
            'success' => false,
            'error_code' => $error_code,
            'error_message' => $array_errors[$error_code]),
            200
            );
        }
        else{
            $mult_id = isset($object->mult_id) ? $object->mult_id: NULL;
        }  



        $image_content = NULL;
        $image_type = NULL;
        if(isset($mult_id)){
            $multimedia = Multimedia::find($mult_id);
            $image_content = $multimedia->mult_content;
            $image_type = $multimedia->mult_content_type;
        }
        

        $response = Response::make($image_content, 200);

        $response->header('Content-Type', $image_type);

        return $response;

    }

    public function setImage(){

        $array_errors = Config::get('ws_errors');
               
        //define parameters validator
        $rules = array(
            'user_id' => 'required|integer|exists:users,user_id',
            'content_type' => 'required_with:image_data',
            'request_type' => 'required|in:user,decision,option',
            'content_id' => 'required|integer',
            'image_data' => 'required_with:content_type',
        );         
        $messages = array(
            'required' => 'The :attribute field is required.',
            'required_with' => 'The :attribute field is required.',
            'exists' => 'The :attribute not exist.',
            'integer' => 'The :attribute must be a integer',
        );
        // doing the validation, passing post data and rules
        $validator = Validator::make(Input::all(), $rules, $messages);  
     
        $array_messages_error = $validator->messages()->getMessages();
        
        $error_message = array();   
        $error_code = 666;
        if ($validator->fails()){
            foreach ($array_messages_error as $key => $value) {
                $error_message[$key] = $value[0];
                $needle = 'field is required';
                $pos = strripos($value[0], $needle);
                //falta algún campo requerido
                if($pos !== false){
                    $error_code = 300;
                    break;
                }
                else{
                    $needle = 'not exist';
                    $pos = strripos($value[0], $needle);
                    if($pos !== false){
                        $error_code = 108;
                        break;
                    }
                    else{
                        $needle = 'must be a integer';
                        $pos = strripos($value[0], $needle);
                        if($pos !== false){
                            $error_code = 107;
                            break;
                        }
                    }
                }  
            }
            return Response::json(array(
                'success' => false,
                'error_code' => $error_code,
                'error_message' => $array_errors[$error_code]),
                200
                );
        }
        //end validation

        //capture the variables
        $user_id = Input::get('user_id', NULL);
        $request_type = Input::get('request_type', NULL);
        $content_id = Input::get('content_id', NULL);
        $content_type = Input::get('content_type', NULL);

        $image_content = Input::get('image_data', NULL);

        $mult_id = NULL;
        $object = NULL;
        switch ($request_type) {
            case 'user':
                $object = User::find($content_id);
                break;
            case 'decision':
                $object = Decision::find($content_id);
            break;
            case 'option':
                $object = Option::find($content_id);
                break;            
            default:
                break;
        }

        if(!isset($object)){
            $error_code = 108;
            return Response::json(array(
            'success' => false,
            'error_code' => $error_code,
            'error_message' => $array_errors[$error_code]),
            200
            );
        }
        else{
            $mult_id = isset($object->mult_id) ? $object->mult_id: NULL;
        }        
                
        if(isset($mult_id)){
            $multimedia = Multimedia::find($mult_id);   
            if(isset($image_content)){
                $multimedia->mult_content = base64_decode($image_content);
                $multimedia->mult_content_type = $content_type;
                $multimedia->save();
            } 
            else{
                $object->mult_id = NULL;
                $object->save();
                $multimedia->delete();
            }        
            
        }
        else{
            $multimedia = new Multimedia();
            $multimedia->mult_content = base64_decode($image_content);
            $multimedia->mult_content_type = $content_type;
            $multimedia->save();
            $mult_id = $multimedia->mult_id;
            $object->mult_id = $mult_id;
            $object->save();
        }

        $object->pushObject();
        return Response::json(array(
            'success' => true,
            'img_update_time' => $multimedia->updated_at->timestamp),
            200
            );

    }
}

?>