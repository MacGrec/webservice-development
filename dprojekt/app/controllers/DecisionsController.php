<?php 

include_once(app_path().'/includes/connectors/GoogleCloud.php');
include_once(app_path().'/includes/tools/Functions.php');

class DecisionsController extends BaseController {

    public function delayed(){
        ini_set('max_execution_time', 0);
        set_time_limit(0);
        $time = Input::get('time', 0);
        sleep($time);
        return Response::json(array(
            'success' => true,),
        200
        );
    }

    public function test(){
        $array_reg_id = array(
            /*"APA91bE6k7Y66Pr0QWNnsvkq6XnfzRnJZOI5CLFBg4gHWIt8KPcRxy0bDiTNqXbDI_DO-mcxPdygGWEwbZ1WpIzETLkmT65UsiU8i8_6xauuvj8vmDQ5rs4LopiI0O5QOAabBsKwDXNG99yUSeCkkLONGAY9U5RqEA",
            "APA91bGaOqHEQUrv2vw1xNChvSAKENS9hgfi5Kki-tVBLw-4FsyhIFZjLo_VIF2RZAOlZPdpXSWlbdw8ioGLVAFzdvqJkfQOoVn2eaBQCIB9YecbOnusvwl6p1mqII8A8QijXqlrxS8FWN0VBlpyyNoXhXPJ5gjOBw",
            "APA91bHpg6c_juevVeCoZTodh6tc_qfLBZ4FoGKV76XIQQP90WqyLPFUsJ-njLe2xmj4INNaqYBW86UTEnWq-aro74qjluaNX7I8p5cczKMOfi4-XwGH-KNAP9freRoeRreQUgmCMyOqZUspjhO4BbFjrkP-UFfk7g",
            "APA91bHMI8VSIG_lX_AGpzMwWYovxj5hWfgZMs9FxfuoSi_rWKPp6m8GdqyOfQZza64GAnc4FL0sDoGXXygd6ndqxhKPt7AVgZaeHz3JoSyJswrlFsJ1P_TAPLliNzoUdqV8INWm0U8xRU0Io4EkINcCxYku-oQ-sQ",
            "APA91bHMI8VSIG_lX_AGpzMwWYovxj5hWfgZMs9FxfuoSi_rWKPp6m8GdqyOfQZza64GAnc4FL0sDoGXXygd6ndqxhKPt7AVgZaeHz3JoSyJswrlFsJ1P_TAPLliNzoUdqV8INWm0U8xRU0Io4EkINcCxYku-oQ-sQ",
            "APA91bEKvo6gZ64OO7lmkUJi0g9kdfo8MHKr9Rzf1jqzvm7dOOypGKAJml3tIDZeaqYqsIgJLXTLnCxO28-UT6PghicQfSDvU3ny6KFufqbuf9eT8SAdXywuTLG0d-otAmYIDDjpTrgL",
            "APA91bFDqRjAMUsp3X_KKtqhhz98BErJM4HMeW444Aj3ho6OmXSlZIQ9tSuQ7amZBiA7MBDtykLsTD0tucbyd6wwULy8aewzOnfDNqMmAuxZvanjVCXIN3--AhmyFzXYWghnaRKm5q7y",
            "APA91bH5pzmV_Cin8EsngC99AENyhs6e3tJNwxqBtpNMR9QYxSgu20UcubiLwkSeyvUUz1NVabhaHhB_mE2E2NwXbJqVB8zO97TdNmXBvGsmxPXUoNu2mTdskYPr4lotL7GpELRS9lds",
            "APA91bHB_mf8aSM84JcB4Rh1g2BK9PxRU3eZz1uCB5gGAKWgn0ZRqzt7mk3hOUputmvvhsgutfMYMO4rJaG9ewHl6i1JZ1uKELAYN2AcBeI9wfPVsf_X_yjT7RKLw_0w_4rRH8-vmRNQPeNo_xLH2fo0YjaTuhbi6g",
            "APA91bHQEOssmPt0-_DNiUvd5_uMsKipAtbxwIQZJIRbtVt1rSr6KT7KU4Tidbc7McsHppc6tXbnMfmJNnrm5ho6X3FvMPsGykPfCq6RkKD9v5BX6nnhtxfdv5kkbDJG95F1jfI2MM4I",
            "APA91bHUK9K9oVhvn3tpbRreM6waTQ0KEI3mWbAcc68X8W6HP2IQTIVEA8epUyMqxCaj57qkgNFE5Qpk3X_M6SzFeiVDk7hZMbifAyFxXw2pn6Ow4DdrTT2FMwWyqiGo-4_AJKQhZCwPDzZsgTKWDuo3yScJHMWQsw",
            "APA91bGJl9zrrTQNvoQ0qPQm17G0nleiX1g5f1Bn0D43GTuVAZH2of7-VDykAWcTmOP3AUfd409IGLTTeQ0-u96EfSDafijGnMA0ZWW3Lki_n4Pu3A_KGuimrxiM-SQjHlVFPsVXIxNbvD-EskBTWkSDol5PbFfgWg",
            "APA91bGaR4lNwx5HreW1pcEttDJNXEsMA-kSrwhuU7_r0DZVeQNXl8_E-S1yH2gviv1VIHA0GDJQ_WtoU6W8GqpkVQlo638V8yUxkgkD8MC3jCTqw9iruI8uOn9kO_NKJl2VIyq6fMBn2dTRUUZLeWmUwFvWR6KQGg",
            */
            "ABC",
            );

        $data = array
        (
            "user_id_admin"=>1,
            "user_ids_participant"=>array
                (
                    2,
                    3,
                    20,
                    45,
                ),
            "title"=>utf8_encode("¿Que personaje de Dragon Ball mola más?"),
            "description"=>utf8_encode("Vamos a ver si nos ponemos de acuerdo entre todos de una vez :)"),
            "options"=>array
            (   
                1=>array
                (
                    "id"=>1,
                    "text"=>"Goku"
                ),
                2=>array
                (
                    "id"=>2,
                    "text"=>"Vegeta"
                ),
                3=>array
                (
                    "id"=>3,
                    "text"=>"Piccolo"
                )
            ),
            "ddl" => 1417448550,
            "type" => 1,
        );
        $return = GoogleCloud::send($array_reg_id, $data);
        return Response::json(array(
                'success' => $return),
                200
                );;

    }

	public function create(){	
		$post = Input::all();	       
   		//array with content of app/config/ws_errors.php
        $array_errors = Config::get('ws_errors');

        $decision_states = Config::get('decision_states');
        $array_participant_status = Config::get('participant_status');


        //start Validate the data

        //Define the validator rules
        $rules = array(
            'user_id' => 'required|integer|exists:users,user_id,deleted_at,NULL',
            'dec_seed' => 'string',
            'title' => 'required',
            'options' => 'required',
            'user_ids_participant' => 'required',
            'ddl' => 'numeric',
            'type' => 'required |integer|exists:decision_types,dec_type_id',

        );

        //define the error messages of the validator
        $messages = array(
            'required' => 'The :attribute field is required.',
            'integer' => 'The :attribute is not a integer.',
            'string' => 'The :attribute is not a string.',
            'exists' => 'The :attribute entered not exist.',
            'numeric' => 'The :attribute must be a number',
        );
        //Creating the validator 
        $validator = Validator::make($post, $rules, $messages);  
     

        $array_messages_error = $validator->messages()->getMessages();
        
        $error_message = array();
        $error_code = 666;
        if ($validator->fails()){
            foreach ($array_messages_error as $key => $value) {
            	$error_message[$key] = $value[0];
                $needle = 'field is required';
                $pos = strripos($value[0], $needle);
                //falta algún campo requerido
                if($pos !== false){
                    $error_code = 300;
                    break;
                }
            	else{                            
                    $needle = 'is not a';
                    $pos = strripos($value[0], $needle);
                    //el email no se encuentra en BBDD
                    if($pos !== false){
                        $error_code = 107;  
                        break;                  
                    }
                    else{
                        $needle = 'must be a number';
                        $pos = strripos($value[0], $needle);
                        if($pos !== false){
                            $error_code = 107;
                            break;
                        }
                        else{
                            $needle = 'not exist';
                            $pos = strripos($value[0], $needle);
                            //el email no se encuentra en BBDD
                            if($pos !== false){
                                $error_code = 108;  
                                break;                  
                            }
                        }                         
                    }                    
                }
            }
            return Response::json(array(
                'success' => false,
                'error_code' => $error_code,
                'error_message' => $array_errors[$error_code]),
                200
                );
        }       
        
        //end Validate the data


        $title = $post["title"];
        $description = isset($post["description"]) ? $post["description"] : null;
        $user_id_admin = $post["user_id"];
        $dec_seed = isset($post["dec_seed"]) ? $post["dec_seed"] : null;
        $options = $post["options"];
        $ddl = isset($post['ddl']) ? $post['ddl'] : null;
        $type = $post["type"]; // TO DO
        $array_user_ids_participant = $post["user_ids_participant"];

        //comprobar si existe ya esa semilla (dec_seed)
        if(isset($dec_seed)){
            $decision = Decision::findDecisionsBySeed($dec_seed);
            if(isset($decision)){
                $error_code = 115;
                return Response::json(array(
                    'success' => false,
                    'error_code' => $error_code,
                    'error_message' => $array_errors[$error_code]),
                    200
                    );
            }
        }
        

        //comprobar que la fecha de ddl es mayor que la fecha de creacion de la decision, como es ahora el now.
        //timestamp of this moment
        $now = time();
        if(isset($ddl) && $now >= $ddl){
            $error_code = 109;
            return Response::json(array(
                'success' => false,
                'error_code' => $error_code,
                'error_message' => $array_errors[$error_code]),
                200
                );
        }

        //check if its a complete decision
        $array_contacts_role = Config::get('contacts_role');        
        $real_participants = array();
        foreach ($array_user_ids_participant as $id_participant) {
            $user = User::find($id_participant);
            if(isset($user) && $user != false && !empty($user)){
                $contact = Contact::findContact($user_id_admin, $id_participant);
                if(isset($contact) && $contact != false && !empty($user) || $id_participant === $user_id_admin){
                    $real_participants[] = $id_participant;
                    
                } 
            }
        }

        //add to the list the participants the admin of the decision if not was in the list
        if(!in_array($user_id_admin,  $real_participants))
            $real_participants[] = $user_id_admin;

        if(count($options) < 1){
            $error_code = 110;
            return Response::json(array(
                'success' => false,
                'error_code' => $error_code,
                'error_message' => $array_errors[$error_code]),
                200
                );
        }

        //transform the timestamp to date format
        $date_ddl = isset($ddl) ? date('Y-m-d H:i:s', $ddl) : null;


        //create the decision
       	$decision = new Decision;
        $decision->decision_title = $title;
        $decision->decision_desc = $description;
        $decision->user_id = $user_id_admin;
        $decision->dec_seed = $dec_seed;
        $decision->dec_stat_id = $decision_states["OPEN"];
        $decision->decision_ddl = $date_ddl; 
        $decision->dec_type_id = $type;
        $decision->save();
        $decision_id = $decision->decision_id;
        $date_array = $decision->updated_at;;
        $updated_at = $date_array->timestamp;

        //if everithing was well, create the other elements.    

        $array_options_return = array();
        $array_options_gcm = array();
        if(is_int($decision_id) &&  intval($decision_id) > 0){
        	//create the options
        	foreach ($options as $option_info) {
        		$option = new Option;
        		$option->option_title = $option_info["text"];
        		$option->decision_id = $decision_id;
        		$option->save();
        		$array_options_return[] = array("client"=>$option_info["id"],"server"=>$option->option_id);
                $array_options_gcm[] = array("id"=>$option->option_id, "text"=>$option_info["text"]);
        	}        	
        }
                        
        //create the participants   


        $array_regs_id = array();
        $return_ids_participants = array();
        Log::info('real participants' .var_export($real_participants, true));
        foreach ($real_participants as $user_id) {
            $gcm_registers = null;
            $participant = new Participant;
            $participant->user_id = $user_id;
            $participant->decision_id = $decision_id;

            if($user_id != $user_id_admin){
                $participant->par_stat_id = isset($array_participant_status["NOT_RECEIVED"])? $array_participant_status["NOT_RECEIVED"]: NULL;
                $array_temp_regs_id = Gcmregister::getRegIDsByUserId($participant->user_id);
                $array_regs_id = array_merge($array_regs_id, $array_temp_regs_id);            
            }
            else{
                $participant->par_stat_id = isset($array_participant_status["READ"])? $array_participant_status["READ"]: NULL;
            }
            Log::info('create participants' .var_export( $participant, true));

            $participant->save();     

            $participant_id = $participant->part_id;
            $return_ids_participants[] = array("user_id"=>$user_id,"participant_id"=>$participant_id);
        }


         $data = array
        (
            "notif_type" => "new_decision",
            "decision_id" => $decision_id,
            "user_id_admin"=>$user_id_admin,
            "title"=>$title,
            "user_ids_participant"=>$return_ids_participants,            
            "options"=>$array_options_gcm,
            "type" => $type,
            "update_time" =>$updated_at,
        );

        if(!isset($date_ddl))
            $data["ddl"] = $date_ddl;
        if(!isset($date_ddl))
            $data["description"] = $description;

        if (count($array_regs_id)>0) {
            //var_dump($gcm_register->user_id);                   
            $return = GoogleCloud::send($array_regs_id, $data);
           //return $return;
            //tratar el return de GCM
         }  
        return Response::json(array(
                'success' => true,
                'decision_id' => $decision_id,
                'options_id' => $array_options_return,                
                'user_ids_participant' => $return_ids_participants),
                200
                );

	}
    
    public function AddParticipant(){   
        $post = Input::all();          
        //array with content of app/config/ws_errors.php
        $array_errors = Config::get('ws_errors');
        $array_participant_status = Config::get('participant_status');


        //start Validate the data

        //Define the validator rules
        //define parameters validator
        $rules = array(
            'user_id' => 'required|integer|exists:users,user_id,deleted_at,NULL',
            'dec_seed' => 'required|string|exists:decisions,dec_seed,deleted_at,NULL',
        );         
        $messages = array(
            'required' => 'The :attribute field is required.',
            'exists' => 'The :attribute not exist.',
            'integer' => 'The :attribute must be a integer',
            'string' => 'The :attribute must be a string',
        );
        // doing the validation, passing post data and rules
        $validator = Validator::make(Input::all(), $rules, $messages);  
     
        $array_messages_error = $validator->messages()->getMessages();
        
        $error_message = array();   
        $error_code = 666;
        if ($validator->fails()){
            foreach ($array_messages_error as $key => $value) {
                $error_message[$key] = $value[0];
                $needle = 'field is required';
                $pos = strripos($value[0], $needle);
                //falta algún campo requerido
                if($pos !== false){
                    $error_code = 300;
                    break;
                }
                else{
                    $needle = 'not exist';
                    $pos = strripos($value[0], $needle);
                    if($pos !== false){
                        $error_code = 108;
                        break;
                    }
                    else{
                        $needle = 'must be a';
                        $pos = strripos($value[0], $needle);
                        if($pos !== false){
                            $error_code = 107;
                            break;
                        }   
                    }
                }  
            }
            return Response::json(array(
                'success' => false,
                'error_code' => "$error_code",
                'error_message' => $array_errors[$error_code].var_export($array_messages_error, true)),
                200
                );
        }
        //end validation

        //capture the variables
        $user_id = Input::get('user_id', NULL);
        $dec_seed = Input::get('dec_seed', NULL);  
            
        
       
        $decision = Decision::findDecisionsBySeed($dec_seed);
        

        $decision_id = $decision->decision_id;
        
        Log::info('la decision' .var_export( $decision, true));

        $participant = Participant::findParticipantByUserID_AND_DecID($user_id, $decision_id);

        $decision_update_at = strtotime($decision->updated_at->toDateTimeString());
        $part_id = null;
        if(!isset($participant)){
            //create the participants 
            $participant = new Participant;
            $participant->user_id = $user_id;
            $participant->decision_id = $decision_id;        
            $participant->par_stat_id = isset($array_participant_status["READ"])? $array_participant_status["READ"]: NULL;
            
            Log::info('create participant' .var_export( $participant, true));

            $participant->save();         
            $part_id = $participant->part_id;
            $decision_update_at = $decision->pushDecision();
        }

        $return_decision = Functions::GetinfoDecision($decision, $decision_update_at, $part_id, true);//Is public 
        
          
        return Response::json($return_decision,
            200
            );

    }


    public function getDecision(){   
        $array_errors = Config::get('ws_errors');
        $array_participant_status = Config::get('participant_status');
        $array_contacts_role = Config::get('contacts_role');       
       
        //define parameters validator
        $rules = array(
            'user_id' => 'required_with:decision_id|integer|exists:users,user_id,deleted_at,NULL',
            'decision_id' => 'required_without:dec_seed|integer|exists:decisions,decision_id,deleted_at,NULL',
            'update_time' => 'numeric',
            'dec_seed' => 'required_without:decision_id|string|exists:decisions,dec_seed,deleted_at,NULL',
            'dec_admin_seed' => 'required_without_all:decision_id,dec_seed|string|exists:decisions,dec_admin_seed,deleted_at,NULL',
        );         
        $messages = array(
            'required_without' => 'The :attribute field is required.',
            'required_without_all' => 'The :attribute field is required.',
            'required' => 'The :attribute field is required.',
            'exists' => 'The :attribute not exist.',
            'integer' => 'The :attribute must be a integer',
            'string' => 'The :attribute must be a string',
            'numeric' => 'The :attribute must be a number',
        );
        // doing the validation, passing post data and rules
        $validator = Validator::make(Input::all(), $rules, $messages);  
     
        $array_messages_error = $validator->messages()->getMessages();
        
        $error_message = array();   
        $error_code = 666;
        if ($validator->fails()){
            foreach ($array_messages_error as $key => $value) {
                $error_message[$key] = $value[0];
                $needle = 'field is required';
                $pos = strripos($value[0], $needle);
                //falta algún campo requerido
                if($pos !== false){
                    $error_code = 300;
                    break;
                }
                else{
                    $needle = 'not exist';
                    $pos = strripos($value[0], $needle);
                    if($pos !== false){
                        $error_code = 108;
                        break;
                    }
                    else{
                        $needle = 'must be a';
                        $pos = strripos($value[0], $needle);
                        if($pos !== false){
                            $error_code = 107;
                            break;
                        }   
                    }
                }  
            }
            return Response::json(array(
                'success' => false,
                'error_code' => "$error_code",
                'error_message' => $array_errors[$error_code].var_export($array_messages_error, true)),
                200
                );
        }
        //end validation

        //capture the variables
        $user_id = Input::get('user_id', NULL);
        $decision_id = Input::get('decision_id', NULL);
        $dec_seed = Input::get('dec_seed', NULL);
        $dec_admin_seed = Input::get('dec_admin_seed', NULL);
        $update_time = Input::get('update_time', NULL);

        $public = true;
        $admin = false;
        $part_id = NULL;
        if(isset($decision_id)){
            $public = false;
            $decision = Decision::find($decision_id);
            $contact = Contact::findContact($user_id,$decision->user_id);
            $contact_opos = Contact::findContact($decision->user_id, $user_id); 
            $blocked_role = $array_contacts_role['I_BLOCKED_ROLE'];
            //if the decision nos exist or if you has blocked the admin of the decision or the opossite
            if(!isset($decision) || empty($decision) || (isset($contact) && $contact->con_ro_id === $blocked_role) ||  (isset($contact_opos) && $contact_opos->con_ro_id === $blocked_role) ){
                $error_code = 108;
                return Response::json(array(
                    'success' => false,
                    'error_code' => $error_code,
                    'error_message' => $array_errors[$error_code]),
                    200
                    );
            }
        }
        elseif (isset($dec_seed)) {        
            $decision = Decision::findDecisionsBySeed($dec_seed);
        }
        elseif (isset($dec_admin_seed)){
            $admin = true;
            $decision = Decision::findDecisionsByAdminSeed($dec_admin_seed);
        }

        Log::info('Es publica  '.var_export($public, true));
        Log::info('La decision  '.var_export($decision, true));
        Log::info('La decision  '.var_export($decision->user_id, true));

        

        if(isset($user_id)){
            $participant =  Participant::findParticipantByUserID_AND_DecID($user_id, $decision_id);
            //set status of participant to readed
            if(isset($participant)){
                $part_id = $participant->part_id;
                $status = intval($participant->par_stat_id);
                if($status === $array_participant_status["NEW"] || $status === $array_participant_status["NOT_RECEIVED"] ){
                    $participant->par_stat_id = isset($array_participant_status["READ"])? $array_participant_status["READ"]: NULL;
                    $participant->updateParticipant();
                    $decision_new_update_at = $decision->pushDecision();

                    $participants_object = Decision::getParticipants($decision_id);
                    $participants_id = array();
                    $array_regs_id = array();
                    foreach ($participants_object as $participant){
                        if($participant->user_id != $user_id){
                           $array_temp_regs_id = Gcmregister::getRegIDsByUserId($participant->user_id);
                           $array_regs_id = array_merge($array_regs_id, $array_temp_regs_id);
                        }
                        $participants_id[] = $participant->user_id;
                    }

                    $data = array
                    (
                        "notif_type" => "part_status_update",
                        "decision_id" => $decision_id,
                        "admin_id" => $decision->user_id,
                        "user_ids_participant" => $participants_id,
                        "user_id"=>$user_id,
                        "part_status" => $array_participant_status["READ"],
                        "update_time" => $decision_new_update_at,
                    );
                    
                    Log::info('Tamaño total array de registros gcm  '.count($array_regs_id));
                    if (count($array_regs_id)>0) {           
                        $return = GoogleCloud::send($array_regs_id, $data);
                     }

                }            
            }
        }            
        
        

        $updated_at = strtotime($decision->updated_at->toDateTimeString());
        
        if(!isset($update_time) || $updated_at > $update_time){
            $return_decision = Functions::GetinfoDecision($decision, $updated_at, $part_id, $public, $admin);
            $error_code = 111;
            $return_decision  = (is_array($return_decision)) ? $return_decision : array('success' => true, 'error_code' => $error_code,'error_message' => $array_errors[$error_code] ) ;
            Log::info('return decision_content  '.var_export($return_decision, true));
            return Response::json($return_decision,
            200
            );
        }  //end if update_time
        else{
            return Response::json(array(
                'success' => true,
                'up_to_date'=>true),
                200
                );
        }

        
    }


    public function getDecisionList(){
         $array_errors = Config::get('ws_errors');
         $array_participant_status = Config::get('participant_status');
         $array_contacts_role = Config::get('contacts_role');       
       
        //define parameters validator
        $rules = array(
            'user_id' => 'required|integer|exists:users,user_id,deleted_at,NULL',
            'amount_decs' => 'required|numeric|min:1',
            'update_time' => 'numeric',
            'update_type' => 'integer|in:1,-1'
        );         
        $messages = array(
            'required' => 'The :attribute field is required.',
            'exists' => 'The :attribute not exist.',
            'integer' => 'The :attribute must be a integer',
            'numeric' => 'The :attribute must be a number',
            'min' => 'The :attribute must be higher than 0',
            'in' => 'The :attribute must be the correct type'
        );
        // doing the validation, passing post data and rules
        $validator = Validator::make(Input::all(), $rules, $messages);  
     
        $array_messages_error = $validator->messages()->getMessages();
        
        $error_message = array();   
        $error_code = 666;
        if ($validator->fails()){
            foreach ($array_messages_error as $key => $value) {
                $error_message[$key] = $value[0];
                $needle = 'field is required';
                $pos = strripos($value[0], $needle);
                //falta algún campo requerido
                if($pos !== false){
                    $error_code = 300;
                    break;
                }
                else{
                    $needle = 'not exist';//expected error
                    $pos = strripos($value[0], $needle);
                    if($pos !== false){
                        $error_code = 108;
                        break;
                    }
                    else{
                        $needle = 'must be a integer';
                        $pos = strripos($value[0], $needle);
                        if($pos !== false){
                            $error_code = 107;
                            break;
                        }
                        else{
                            $needle = 'must be a number';
                            $pos = strripos($value[0], $needle);
                            if($pos !== false){
                                $error_code = 107;
                                break;
                            }
                            else{
                                $needle = 'must be higher than 0';
                                $pos = strripos($value[0], $needle);
                                if($pos !== false){
                                    $error_code = 107;
                                    break;
                                }  
                                else{
                                    $needle = 'must be the correct type';
                                    $pos = strripos($value[0], $needle);
                                    if($pos !== false){
                                        $error_code = 113;
                                        break;
                                    }
                                }                          
                            }                          
                        }   
                    }
                }  
            }
            return Response::json(array(
                'success' => false,
                'error_code' => $error_code,
                'error_message' => $array_errors[$error_code]),
                200
                );
        }
        //end validation

        //capture the variables
        $user_id = Input::get('user_id', NULL);
        $amount_decs = Input::get('amount_decs', NULL);
        $update_time = Input::get('update_time', NULL);
        $update_type = Input::get('update_type', NULL);
         
        $decisions_deleted = array();
        $decisions = array();
        if($update_type === 1 || !isset($update_type)){
            if(isset($update_time)){
            //transform the timestamp to date format
            $update_timestamp = isset($update_time) ? date('Y-m-d H:i:s', $update_time) : null;

            $decisions =  DB::select( DB::raw("SELECT decisions.*, participants.part_exited, participants.par_stat_id
                FROM decisions 
                LEFT JOIN participants ON decisions.decision_id = participants.decision_id 
                WHERE 
                    participants.user_id = $user_id 
                    AND participants.part_exited = 0 
                    AND decisions.updated_at > '$update_timestamp' 
                    AND decisions.deleted_at IS NULL
                 ORDER BY decisions.updated_at 
                 DESC LIMIT $amount_decs")); 

            $decisions_deleted =  DB::select( DB::raw("SELECT decisions.*, participants.part_exited, participants.par_stat_id
                FROM decisions 
                LEFT JOIN participants 
                ON decisions.decision_id = participants.decision_id 
                WHERE
                    participants.user_id = $user_id AND decisions.updated_at > '$update_timestamp' 
                    AND (decisions.deleted_at IS NOT NULL
                    OR  participants.part_exited = 1)
                ORDER BY decisions.updated_at DESC")); 

            Log::info('Decision en 1  '.var_export($decisions, true));
           

            }
            else{
                $decisions =  DB::select( DB::raw("SELECT decisions.*, participants.part_exited, participants.par_stat_id
                    FROM decisions
                    LEFT JOIN participants 
                    ON decisions.decision_id = participants.decision_id
                    WHERE 
                        participants.user_id = $user_id 
                        AND participants.part_exited = 0 
                        AND decisions.deleted_at IS NULL
                    ORDER BY decisions.updated_at 
                    DESC LIMIT $amount_decs")); 

                Log::info('Decision en 2'.var_export( $decisions, true));


            } 

        }
        elseif ($update_type === -1 && isset($update_time)) {
            //transform the timestamp to date format
            $update_timestamp = isset($update_time) ? date('Y-m-d H:i:s', $update_time) : null;

            $decisions = DB::select( DB::raw("SELECT decisions.*, participants.part_exited, participants.par_stat_id
                FROM decisions 
                LEFT JOIN participants 
                ON decisions.decision_id = participants.decision_id 
                WHERE 
                    participants.user_id = $user_id 
                    AND participants.part_exited = 0 
                    AND decisions.updated_at < '$update_timestamp' 
                    AND decisions.deleted_at IS NULL 
                ORDER BY decisions.updated_at 
                DESC LIMIT $amount_decs"));   

                Log::info('Decision en 3'.var_export( $decisions, true));
        
            
        }        
               
         Log::info('antes de count'.var_export( $decisions, true));
        if(count($decisions) == 0 && count($decisions_deleted) == 0){
            return Response::json(array(
                'success' => true,
                'up_to_date' => true),
                200
                );
        }
        else{
            /*$updated_at = isset($decisions[0]) ? strtotime($decisions[0]->updated_at) : null;
            $updated_at = isset($decisions_deleted[0]) ? strtotime($decisions_deleted[0]->updated_at) : null;*/
            $decisions_return = array();
            $decisions_deleted_return = array();            
            foreach ($decisions as $decision) {
                $blocked_role = $array_contacts_role['I_BLOCKED_ROLE'];
                $admin_id = $decision->user_id;
                //if the user_id who is asking for the list o decision has bloked to the decision's admin then the user_id is not receiving the decision
                $contact = Contact::findContact($user_id, $admin_id);
                Log::info('user_id-> '.$user_id." admin-> ".$admin_id);
                Log::info('contact get_decision_list: '.var_export( $contact, true));
                if (!isset($contact) || $contact->con_ro_id != $blocked_role ) {
                    $decision_content = array();
                    $updated_at = strtotime($decision->updated_at);
                    $participant = Participant::findParticipantByUserID_AND_DecID($user_id, $decision->decision_id);
                    //set status of participant to NEW if was NOT_RECEIVED
                    if(isset($participant)){
                        $status = intval($participant->par_stat_id);
                        if($status === $array_participant_status["NOT_RECEIVED"]){
                            $participant->par_stat_id = isset($array_participant_status["NEW"])? $array_participant_status["NEW"]: NULL;
                            $participant->updateParticipant();
                            $decision_object = new Decision();
                            $decision_object->decision_id = $decision->decision_id;
                            $decision_object->pushDecision();



                            $participants_object = Decision::getParticipants($decision->decision_id);
                            $participants_id = array();
                            $array_regs_id = array();
                            foreach ($participants_object as $participant){
                                if($participant->user_id != $user_id){
                                   $array_temp_regs_id = Gcmregister::getRegIDsByUserId($participant->user_id);
                                   $array_regs_id = array_merge($array_regs_id, $array_temp_regs_id);
                                }
                                $participants_id[] = $participant->user_id;
                            }

                            $data = array
                            (
                                "notif_type" => "part_status_update",
                                "decision_id" => $decision->decision_id,
                                "admin_id" => $decision->user_id,
                                "user_ids_participant" => $participants_id,
                                "user_id"=>$user_id,
                                "part_status" => $array_participant_status["NEW"],
                                "update_time" => $update_time,
                            );
                            
                            Log::info('Tamaño total array de registros gcm  '.count($array_regs_id));
                            if (count($array_regs_id)>0) {           
                                $return = GoogleCloud::send($array_regs_id, $data);
                             }


                        }
                        
                    } 
                    $decisions_return[]= Functions::GetinfoDecision($decision, $updated_at, $user_id); 
                    
                }

                

            }

            foreach ($decisions_deleted as $decision) {
                $conten_decision = array();
                $updated_at = strtotime($decision->updated_at);
                $decisions_deleted_return[] = array("decision_id"=>$decision->decision_id,"admin_id"=>$decision->user_id,
                    "update_time"=>$updated_at);
            }
            Log::info('Decision antes de devolver el response: '.var_export( $decisions_return, true));
            return Response::json(array(
                'success' => true,
                'decisions' => $decisions_return,
                'decisions_deleted' => $decisions_deleted_return),
                200
                );
        }

    }

    public function deleteDecision(){
        Log::info('delete Decision');
        $array_errors = Config::get('ws_errors');
       
        //define parameters validator
        $rules = array(
            'user_id' => 'required|integer|exists:users,user_id,deleted_at,NULL',
            'decision_id' => 'required|integer|exists:decisions,decision_id,deleted_at,NULL',
        );         
        $messages = array(
            'required' => 'The :attribute field is required.',
            'exists' => 'The :attribute not exist.',
            'integer' => 'The :attribute must be a integer.',
        );
        // doing the validation, passing post data and rules
        $validator = Validator::make(Input::all(), $rules, $messages);  
     
        $array_messages_error = $validator->messages()->getMessages();
        
        $error_message = array();   
        $error_code = 666;
        if ($validator->fails()){
            foreach ($array_messages_error as $key => $value) {
                $error_message[$key] = $value[0];
                $needle = 'field is required';
                $pos = strripos($value[0], $needle);
                //falta algún campo requerido
                if($pos !== false){
                    $error_code = 300;
                    break;
                }
                else{
                    $needle = 'The decision_id not exist';
                    $pos = strripos($value[0], $needle);
                    if($pos !== false){
                        return Response::json(array(
                        'success' => true),
                        200
                        ); 
                    }
                    $needle = 'not exist';//expected error
                    $pos = strripos($value[0], $needle);
                    if($pos !== false){
                        $error_code = 108;
                        break;
                    }
                    else{
                        $needle = 'must be a integer';
                        $pos = strripos($value[0], $needle);
                        if($pos !== false){
                            $error_code = 107;
                            break;
                        }  
                    }
                }  
            }
            return Response::json(array(
                'success' => false,
                'error_code' => $error_code,
                'error_message' => $array_errors[$error_code]),
                200
                );
        }
        //end validation

        //capture the variables
        $user_id = Input::get('user_id', NULL);
        $decision_id = Input::get('decision_id', NULL);

        Functions::complexDeleteDecisions(array($user_id), array($decision_id));

        $participants_object = Decision::getParticipants($decision_id);

        $array_regs_id = array();
        foreach ($participants_object as $participant){
            if($participant->user_id != $user_id){
               $array_temp_regs_id = Gcmregister::getRegIDsByUserId($participant->user_id);
               $array_regs_id = array_merge($array_regs_id, $array_temp_regs_id);
            }
        }

        $data = array
        (
            "notif_type" => "decision_deleted",
            "decision_id" => $decision_id,
            "user_id"=>$user_id,
        );

        Log::info('Tamaño total array de registros gcm  '.count($array_regs_id));
        if (count($array_regs_id)>0) {           
            $return = GoogleCloud::send($array_regs_id, $data);
         }

        return Response::json(array(
            'success' => true),
            200
            );         
        
    }


    public function send_preference(){
        Log::info('send_preference ');
        $array_errors = Config::get('ws_errors');
        $array_participant_status = Config::get('participant_status');
       
        //define parameters validator
        $rules = array(
            'user_id' => 'integer|exists:users,user_id,deleted_at,NULL',
            'participant_name' => 'string|max:30',
            'participant_id' => 'required_without_all:user_id,participant_name|integer|exists:participants,part_id,deleted_at,NULL',
            'decision_id' => 'required|integer|exists:decisions,decision_id,deleted_at,NULL',
            'options'=> 'required',
        );         
        $messages = array(
            'required_without' => 'The :attribute field is required.',
            'required_without_all' => 'The :attribute field is required.',
            'required' => 'The :attribute field is required.',
            'exists' => 'The :attribute not exist.',
            'integer' => 'The :attribute must be a integer.',
            'string' => 'The :attribute must be a string',
            'max' => 'The :attribute is oversized',
        );
        // doing the validation, passing post data and rules
        $validator = Validator::make(Input::all(), $rules, $messages);  
     
        $array_messages_error = $validator->messages()->getMessages();
        
        $error_message = array();   
        $error_code = 666;
        if ($validator->fails()){
            foreach ($array_messages_error as $key => $value) {
                $error_message[$key] = $value[0];
                $needle = 'field is required';
                $pos = strripos($value[0], $needle);
                //falta algún campo requerido
                if($pos !== false){
                    $error_code = 300;
                    break;
                }
                else{
                    $needle = 'not exist';//expected error
                    $pos = strripos($value[0], $needle);
                    if($pos !== false){
                        $error_code = 108;
                        break;
                    }
                    else{
                        $needle = 'must be a';
                        $pos = strripos($value[0], $needle);
                        if($pos !== false){
                            $error_code = 107;
                            break;
                        }
                        else{
                            $needle = 'is oversized';
                            $pos = strripos($value[0], $needle);
                            if($pos !== false){
                                $error_code = 117;
                                break;
                            }  
                        }
                    }
                    

                }  
            }
            return Response::json(array(
                'success' => false,
                'error_code' => $error_code,
                'error_message' => $array_errors[$error_code]),
                200
                );
        }
        

        //capture the variables
        $user_id = Input::get('user_id', NULL);
        $part_name = Input::get('participant_name', NULL);
        $decision_id = Input::get('decision_id', NULL);
        $options = Input::get('options', NULL);
        $part_id = Input::get('participant_id', NULL);

        $participant = new Participant();
        if(isset($user_id)){
            $participant = Participant::findParticipantByUserID_AND_DecID($user_id, $decision_id);
            //set status of participant to preferences sent
            if(isset($participant)){
                $participant->par_stat_id = isset($array_participant_status["PREFS_SENT"])? $array_participant_status["PREFS_SENT"]: $participant->par_stat_id;
                   
            } 
            

            if(!isset($participant) || empty($participant)){
                $error_code = 111;
                return Response::json(array(
                    'success' => false,
                    'error_code' => $error_code,
                    'error_message' => $array_errors[$error_code]),
                    200
                    );
            }//end validation
        }
        elseif(isset($part_name) && !isset($part_id)){            
            $participant->decision_id = $decision_id;
            $participant->part_nm = $part_name;
            $participant->par_stat_id = isset($array_participant_status["PREFS_SENT"])? $array_participant_status["PREFS_SENT"]: 4;
            
        }
        elseif (isset($part_id)) {
            $participant = Participant::find($part_id);
            $participant->part_nm = isset($participant_name)? $participant_name: $participant->part_nm;
        }

        $participant->save(); 
        $part_id = $participant->part_id;
        

        $timestamp = time();
        foreach ($options as $option) {
            $option_id = $option["option_id"];
            $option_preference = isset($option["option_preference"]) ? $option["option_preference"]: NULL;
            $option_object = Option::find($option_id);
            if(isset($option_object) && !empty($option_object)){
                $preference = Preference::findPreference($part_id, $option_id);
                if(isset($preference) && !empty($preference)){
                    $preference->preference_value = $option_preference;
                    $timestamp = $preference->updatePreference();
                }
                else{
                    $preference = new Preference();
                    $preference->part_id = $part_id;
                    $preference->option_id = $option_id;
                    $preference->preference_value = $option_preference;
                    $preference->save();
                }
            }
        }

        $decision = Decision::find($decision_id);
        $decision->pushDecision();

        if(!isset($preference)){
            //any option exist in the server
            $error_code = 000;
            return Response::json(array(
                'success' => false,
                'error_code' => $error_code,
                'error_message' => $array_errors[$error_code]),
                200
                );
        }

        $participants_object = Decision::getParticipants($decision_id);
        
        $array_regs_id = array();
        $participants_id = array();

        if(isset($user_id)){
            foreach ($participants_object as $participant){
                $exist = Contact::isContact($participant->user_id, $user_id);
                if($participant->user_id != $user_id && (count($participants_object) <= 8 || $exist)){//limitación a 8 notificaciones o si es contacto
                   $array_temp_regs_id = Gcmregister::getRegIDsByUserId($participant->user_id);
                   $array_regs_id = array_merge($array_regs_id, $array_temp_regs_id);
                }
                $participants_id[] = $participant->user_id;
                }

                Log::info('Tamaño total array de registros gcm  '.count($array_regs_id));

                $user = User::find($user_id);

                if (count($array_regs_id)>0) {
                $data = array
                (
                    "notif_type" => "prefs_sent",
                    "decision_id" => $decision_id,
                    "user_id_admin"=> $decision->user_id,                    
                    "title"=> $decision->decision_title,
                    "user_ids_participant"=> $participants_id,
                    "type"=>$decision->dec_type_id,
                    "user_id"=> $user_id,
                    "participant_id" => $part_id,
                    "user_name"=>$user->user_nm,
                    "user_last_name"=> $user->user_last_nm,
                    "options"=> $options,
                    "update_time"=>$timestamp,
                );

                if(isset($decision->decision_desc))
                    $data["description"] = $decision->decision_desc;

                      
                $return = GoogleCloud::send($array_regs_id, $data);
                }
        }
        

        return Response::json(array(
                'success' => true,
                'participant_id' => $part_id,
                'update_time' => $timestamp),
                200
                );

    }

    public function updateDecision(){   
        $array_errors = Config::get('ws_errors');
       
        //define parameters validator
        $rules = array(
            'user_id' => 'required|integer|exists:users,user_id,deleted_at,NULL',
            'decision_id' => 'required|integer|exists:decisions,decision_id,deleted_at,NULL',
            'state' => 'required|integer|exists:decision_states,dec_stat_id',
        );         
        $messages = array(
            'required' => 'The :attribute field is required.',
            'exists' => 'The :attribute not exist.',
            'integer' => 'The :attribute must be a integer',
        );
        // doing the validation, passing post data and rules
        $validator = Validator::make(Input::all(), $rules, $messages);  
     
        $array_messages_error = $validator->messages()->getMessages();
        
        $error_message = array();   
        $error_code = 666;
        if ($validator->fails()){
            foreach ($array_messages_error as $key => $value) {
                $error_message[$key] = $value[0];
                $needle = 'field is required';
                $pos = strripos($value[0], $needle);
                //falta algún campo requerido
                if($pos !== false){
                    $error_code = 300;
                    break;
                }
                else{
                    $needle = 'not exist';
                    $pos = strripos($value[0], $needle);
                    if($pos !== false){
                        $error_code = 108;
                        break;
                    }
                    else{
                        $needle = 'must be a integer';
                        $pos = strripos($value[0], $needle);
                        if($pos !== false){
                            $error_code = 107;
                            break;
                        }
                        else{
                            $needle = 'must be a number';
                            $pos = strripos($value[0], $needle);
                            if($pos !== false){
                                $error_code = 107;
                                break;
                            }                            
                        }   
                    }
                }  
            }
            return Response::json(array(
                'success' => false,
                'error_code' => "$error_code",
                'error_message' => $array_errors[$error_code]),
                200
                );
        }
        //end validation

        //capture the variables
        $user_id = Input::get('user_id', NULL);
        $decision_id = Input::get('decision_id', NULL);
        $state = Input::get('state', NULL);

        $decision_states = Config::get('decision_states');

        $decision = Decision::find($decision_id);
        
        if(!isset($decision) || empty($decision)){
            $error_code = 108;
            return Response::json(array(
                'success' => false,
                'error_code' => $error_code,
                'error_message' => $array_errors[$error_code]),
                200
                );
        }

        if($decision->user_id != $user_id){
            $error_code = 114;
            return Response::json(array(
                'success' => false,
                'error_code' => $error_code,
                'error_message' => $array_errors[$error_code]),
                200
                );
        }

        $decision->dec_stat_id = $state;
        $decision->save();

        $participants_object = Decision::getParticipants($decision_id);

        $array_regs_id = array();
        $participants_id = array();
        foreach ($participants_object as $participant){
            if($participant->user_id != $user_id){
               $array_temp_regs_id = Gcmregister::getRegIDsByUserId($participant->user_id);
               $array_regs_id = array_merge($array_regs_id, $array_temp_regs_id);
            }
            $participants_id[] = $participant->user_id;
        }

        $date_array = $decision->updated_at;
        $update_time = $date_array->timestamp;
                

        $data = array(
            "notif_type" => "decision_state_update",
            "decision_id" => $decision_id,
            "user_id_admin"=>$user_id,
            "title"=> $decision->decision_title,           
            "user_ids_participant"=>$participants_id,
            "state" =>$state,
            "type"=>$decision->dec_type_id,
            "update_time"=>$update_time,
        );

        if(isset($decision->decision_desc))
            $data["description"] = $decision->decision_desc;


        if (count($array_regs_id)>0) {           
            $return = GoogleCloud::send($array_regs_id, $data);
         }

        return Response::json(array(
            'success' => true),
            200
            );
        
    }
    
}
?>