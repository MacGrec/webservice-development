<?php 

include_once(app_path().'/includes/connectors/GoogleCloud.php');
include_once(app_path().'/includes/classes/Image.php');
include_once(app_path().'/controllers/DecisionsController.php');
include_once(app_path().'/includes/tools/Functions.php');
include_once(app_path().'/includes/connectors/GoogleAPIClient.php');

class AppsController extends BaseController {    


    public function SaveFeedback(){
    	$array_errors = Config::get('ws_errors');

        $rules = array(
            'user_id' => 'required',            
            'feedback_type' => 'required|in:BUG,MISSING,IMPROVEMENT,OTHER,NONE,DESIGN',    
            'feedback_text' => 'required',
            'anonymous' => 'required|boolean',       
        );
 
        $messages = array(
            'required' => 'The :attribute field is required.',
            'in' => 'The :attribute must be the correct type',
            'boolean' => 'The :attribute must be a boolean',
        );
        // doing the validation, passing post data and rules
        $validator = Validator::make(Input::all(), $rules, $messages);  
     
        $array_messages_error = $validator->messages()->getMessages();
        Log::info('Signin Validator');
        $error_message = array();   
        $error_code = 666;
        if ($validator->fails()){
            foreach ($array_messages_error as $key => $value) {
                $error_message[$key] = $value[0];
                $needle = 'field is required';
                $pos = strripos($value[0], $needle);
                //falta algún campo requerido
                if($pos !== false){
                    $error_code = 300;
                    break;
                }
                else{
                    $needle = 'must be the correct type';
                    $pos = strripos($value[0], $needle);
                    if($pos !== false){
                        $error_code = 113;
                        break;
                    }
                    else{
	                    $needle = 'must be a boolean';
	                    $pos = strripos($value[0], $needle);
	                    if($pos !== false){
	                        $error_code = 113;
	                        break;
	                    }
                	} 
                }  
            }
            return Response::json(array(
            'success' => false,
            'error_code' => $error_code,
            'error_message' => $array_errors[$error_code]),
            200
            );          
            
         }  
       
        $user_id = Input::get('user_id', NULL);
        $feedback_type = Input::get('feedback_type', NULL);
        $feedback_text = Input::get('feedback_text', NULL);
        $anonymous = Input::get('anonymous', NULL);
        $info = Input::get('info', NULL);

        $feedback = new Feedback;
        $feedback->feedback_type = $feedback_type;
        $feedback->feedback_text = $feedback_text;

        if($anonymous === 'false' || !$anonymous){
        	$feedback->user_id = $user_id;
            $user_object = User::find($user_id);
        }


        if(isset($info)){
        	$feedback->platform = isset($info['platform'])? $info['platform'] : NULL;
	    	$feedback->operating_system = isset($info['operating_system'])? $info['operating_system'] : NULL;
	    	$feedback->os_version = isset($info['os_version'])? $info['os_version'] : NULL;
	    	$feedback->app_version = isset($info['app_version'])? $info['app_version'] : NULL;
        }
        
        $feedback->save();

        $user_string = "Anonimo";
        $user_email = "notemail@diggerapp.com";
        if(isset($user_object)){
            $user_string = $user_object->user_nm." ". $user_object->user_last_nm;
            $user_email = $user_object->user_email;
        }


        //send the feeback to support@diggerapp.com
        /*$content = "Usuario: ".$user_string.
        "Tipo de feedback: ".$feedback_type
                   ."Contenido: ". $feedback_text;

        $mail = "gregorio@diggerapp.com";
        $subject = "[FEEDBACK]";

        $headers = 'From: '.$user_object->user_email;

        mail($mail, $subject, $content, $headers);*/

        $support_mail = "support@diggerapp.com";
        $subject_type = strtoupper($feedback_type);
        $subject = "[Diggerapp FEEDBACK: ".$subject_type."]";
        
        $data = array(
            'user'=>$user_string,
            'type'  => $feedback_type,
            'text'=>$feedback_text,
            'user_email'=> $user_email,
        );


        $info = array(
            'from'=>$user_email,
            'to'=>$support_mail,
            "user_name" =>$user_string,
            "subject" => $subject,
            
        );


        Mail::queueOn('send_feedback','emails.feedback', $data, function($message) use ($info)
        {
           $message->from($info['from'], $info['user_name']);

           $message->to($info['to'], 'Digger support')->subject($info['subject']);
            
        });

       // mail($support_mail, $subject, $subject);

      
         return Response::json(array(
            'success' => true,),
            200
            );    

	
	}

    public function GetInfoFeedback(){
        $feedbacks = Feedback::all();
        $infoFeedback = array();
        foreach ($feedbacks as $feedback) {
            $user_name = "Anonimo";
            $user_email = NULL;
            $user_id = isset($feedback->user_id)? $feedback->user_id : NULL;
            if(isset($user_id)){
                $user_object = User::find($user_id);
                if (isset($user_object)) {
                    $user_name = $user_object->user_nm." ". $user_object->user_last_nm;
                    $user_email = $user_object->user_email;
                }
                
            }
            $type = isset($feedback->feedback_type)? $feedback->feedback_type : "";
            $text = isset($feedback->feedback_text)? $feedback->feedback_text : "";            
            $platform = isset($feedback->platform)? $feedback->platform : "";
            $operating_system = isset($feedback->operating_system)? $feedback->operating_system : "";
            $os_version = isset($feedback->os_version)? $feedback->os_version : "";
            $app_version = isset($feedback->app_version)? $feedback->app_version : "";
            $created = isset($feedback->created_at)?  strtotime($feedback->created_at->toDateTimeString()) : "";
           

            $infoFeedback[] = array(
                "user_id"=> $user_id,
                "user_name"=> $user_name,
                "user_email"=> $user_email,
                "type"=> $type,
                "text"=> $text,
                "platform"=> $platform,
                "operating_system"=> $operating_system,
                "os_version"=>$os_version,
                "app_version"=>$app_version,
                "created"=> $created,
                );
        }
            

        return Response::json(array(
                'success' => true,
                'feedbacks'=> $infoFeedback),
                200
                );
    }
    
}