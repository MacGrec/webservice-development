<?php 

include_once(app_path().'/includes/classes/Crypt.php');
include_once(app_path().'/includes/connectors/GoogleAPIClient.php');

class TestController extends BaseController {

    public function SigIn(){
        $token = Input::get('token', NULL);
        $data = GoogleAPIClient::getDataFromToken($token);

        return Response::json(array(
                'token' => $token,
                'data' => $data),
                200
                );
    }


    public function testGoogleAPI(){
        //$token = "eyJhbGciOiJSUzI1NiIsImtpZCI6IjhiYzY2MjRjMTg4ZmNkYTVjOTJkMmJkNzE3MmQ3YzQyMzZhNWVhN2YifQ.eyJpc3MiOiJhY2NvdW50cy5nb29nbGUuY29tIiwic3ViIjoiMTAxODY2MjU4NjQ0NTI0MDY3NjExIiwiYXpwIjoiODQzODU0MTU2MTI5LTU1cnYyMmZuMDI2YnVqcmIyaXA0cDltYzFnNHZuaXU4LmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tIiwiZW1haWwiOiJlZnJlbC52MkBnbWFpbC5jb20iLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwiYXVkIjoiODQzODU0MTU2MTI5LTlldWd0MDMxZThwZnRjcGNuN3NrdXIzZ3U1NDFwcXFnLmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tIiwiaWF0IjoxNDIzNTI2Mzk4LCJleHAiOjE0MjM1MzAyOTh9.hLxP0bZuWqeDZcDB-qGOIDJYWZB38OT4WO3hOCcwH-ApFaWoIbm72-EHvyqLPP_xsvCWfMxwCTZ4SKznNDHqmnbCGz5jqliV41mHgp7HfVFBrIoYrZPLJgCcPQMcRtn0YWrD_YxmJu3kbGH6Vzt4yS2_TGAX-D-B_OVKq38Xe9g";
        
        $token = Input::get('token', NULL);
        $data = GoogleAPIClient::getDataFromToken($token);

        return Response::json(array(
                'token' => $token,
                'data' => $data),
                200
                );
    }

    public function testCrypt(){
        $keys = array();
        $keys = Crypt::generatekeys();
        echo "Private key: <br><br>";
        echo $keys['private'];
        echo "<br><br><br><br>Public key: <br><br>";
        echo $keys['public'];

        $input = "Vamos a codificar :D";
        $encoded = Crypt::openssl_encrypt($input, $keys['private']);

        var_dump($encoded);

        $decoded = Crypt::openssl_decrypt($encoded, $keys['public']);

        var_dump($decoded);
        
    }
}