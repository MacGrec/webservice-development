<?php 

include_once(app_path().'/includes/connectors/GoogleCloud.php');
include_once(app_path().'/includes/classes/Image.php');
include_once(app_path().'/controllers/DecisionsController.php');
include_once(app_path().'/includes/tools/Functions.php');
include_once(app_path().'/includes/connectors/GoogleAPIClient.php');
include_once(app_path().'/includes/connectors/FacebookAPIClient.php');

class UsersController extends BaseController {    


    public function SigIn(){
        
        $array_errors = Config::get('ws_errors');

        $rules = array(
            'token' => 'required',            
            //'reg_id' => 'required',    
            'name' => 'required_with:last_name',
            'last_name' => 'required_with:name',   
            'social_type' => 'in:facebook,google',
        );
 
        $messages = array(
            'required' => 'The :attribute field is required.',
            'required_with' => 'The :attribute field is required.',
        );
        // doing the validation, passing post data and rules
        $validator = Validator::make(Input::all(), $rules, $messages);  
     
        $array_messages_error = $validator->messages()->getMessages();
        Log::info('Signin Validator');
        $error_message = array();   
        $error_code = 666;
        if ($validator->fails()){
            foreach ($array_messages_error as $key => $value) {
                $error_message[$key] = $value[0];
                $needle = 'field is required';
                $pos = strripos($value[0], $needle);
                //falta algún campo requerido
                if($pos !== false){
                    $error_code = 300;
                    break;
                }  
            }
            return Response::json(array(
            'success' => false,
            'error_code' => $error_code,
            'error_message' => $array_errors[$error_code]),
            200
            );          
            
         }  
       
        $token = Input::get('token', NULL);
        $gcm_reg_id = Input::get('reg_id', NULL);
        $name = Input::get('name', NULL);
        $last_name = Input::get('last_name', NULL);    
        $social_type = Input::get('social_type', "google");    

        
        switch ($social_type) {
            case 'google':
                Log::info('Signin before GoogleAPIClient: '.var_export($token, true) );
                $data = GoogleAPIClient::getDataFromToken($token);
                break;
            case 'facebook':
            Log::info('Signin before FacebookAPIClient: '.var_export($token, true) );
                $data = FacebookAPIClient::getDataFromToken($token);
                break;
            /*default:
                # code...
                break;*/
        }
        
        Log::info('Signin ater GoogleAPIClient: '.var_export($data, true) );
        if(isset($data["error_message"])){
            //verify token failed
            $error_code = 600;
            return Response::json(array(
                'success' => false,
                'error_code' => $error_code,
                'error_message' => $array_errors[$error_code].". ".$data["error_message"]),
                200
                );
        }
        elseif (isset($data["email"])){
            //verify token succesful
            $email = $data["email"];
            $user = User::where(array('user_email'=>$email))->first();

            Log::info('Signin. exist email: '.var_export($user, true) );
            
            if(isset($user) && $user != false && !empty($user)){                
                $user_id = $user->user_id;
                $return = GoogleCloud::save($user_id, $gcm_reg_id);
                Log::info('Signin. GCM guardado:'. $gcm_reg_id .': '.var_export($return, true) );
                $response = array(
                    'success' => true,
                    'user_id' => $user_id,
                    'user_name'=> $user->user_nm,
                    'user_last_name'=>$user->user_last_nm);

                $mult_id = $user->mult_id;
                if(isset($mult_id)){
                    $multimedia = Multimedia::find($mult_id);
                    $date_array = $multimedia->updated_at;
                    $response['img_update_time'] = $date_array->timestamp;
                }

                Log::info('User existe. Logueo signin: '.var_export($response, true) );
                return Response::json($response,
                    200
                    );                                 
                               
            }
            else{
                //email no existe en la BBDD.
                if(!isset($name) || !isset($last_name)){
                    $error_code = 103;
                    return Response::json(array(
                        'success' => false,
                        'error_code' => $error_code,
                        'error_message' => $array_errors[$error_code]),
                        200
                        );
                }
                else{
                    //Registrar al usuario
                    $user = new User;
                    $user->user_email = $email;
                    $user->user_nm = $name;
                    $user->user_last_nm = $last_name;
                    $user->save();
                    $user_id = $user->user_id;

                    $return = GoogleCloud::save($user_id, $gcm_reg_id);
                    $response = array(
                        'success' => true,
                        'user_id' => $user_id,
                        'user_name'=> $user->user_nm,
                        'user_last_name'=>$user->user_last_nm);

                    $mult_id = $user->mult_id;
                    if(isset($mult_id)){
                        $multimedia = Multimedia::find($mult_id);
                        $date_array = $multimedia->updated_at;
                        $response['img_update_time'] = $date_array->timestamp;
                    }

                    Log::info('User registrado. Logueo signin: '.var_export($response, true) );

                    //Digger team has all the user as friends
                    Functions::diggerFriendly($user_id);
                    
                    return Response::json($response,
                        200
                        );                     
                }
                
            }    

        }
        else{
            //there are not email in the token
            $error_code = 601;
            return Response::json(array(
                'success' => false,
                'error_code' => $error_code,
                'error_message' => $array_errors[$error_code]),
                200
                );

        }
    }

    public function WebSigIn(){
        
        $array_errors = Config::get('ws_errors');

        $rules = array(
            'token' => 'required',       
        );
 
        $messages = array(
            'required' => 'The :attribute field is required.',
        );
        // doing the validation, passing post data and rules
        $validator = Validator::make(Input::all(), $rules, $messages);  
     
        $array_messages_error = $validator->messages()->getMessages();
        Log::info('WebSignin Validator');
        $error_message = array();   
        $error_code = 666;
        if ($validator->fails()){
            foreach ($array_messages_error as $key => $value) {
                $error_message[$key] = $value[0];
                $needle = 'field is required';
                $pos = strripos($value[0], $needle);
                //falta algún campo requerido
                if($pos !== false){
                    $error_code = 300;
                    break;
                }  
            }
            return Response::json(array(
            'success' => false,
            'error_code' => $error_code,
            'error_message' => $array_errors[$error_code]),
            200
            );          
            
         }  
       
        $token = Input::get('token', NULL);   

        Log::info('WebSignin before GoogleAPIClient: '.var_export($token, true) );
        $data = GoogleAPIClient::getDataFromToken($token);
        Log::info('WebSignin ater GoogleAPIClient: '.var_export($data, true) );
        if(isset($data["error_message"])){
            //verify token failed
            $error_code = 600;
            return Response::json(array(
                'success' => false,
                'error_code' => $error_code,
                'error_message' => $array_errors[$error_code].". ".$data["error_message"]),
                200
                );
        }
        elseif (isset($data["email"])){
            //verify token succesful
            $email = $data["email"];
            $user = User::where(array('user_email'=>$email))->first();

            Log::info('Websignin. exist email: '.var_export($user, true) );
            
            if(isset($user) && $user != false && !empty($user)){                
                $user_id = $user->user_id;                
                $response = array(
                    'success' => true,
                    'user_id' => $user_id,
                    'user_name'=> $user->user_nm,
                    'user_last_name'=>$user->user_last_nm);
                $mult_id = $user->mult_id;
                if(isset($mult_id)){
                    $multimedia = Multimedia::find($mult_id);
                    $date_array = $multimedia->updated_at;
                    $response['img_update_time'] = $date_array->timestamp;
                }

                Log::info('User existe. Logueo Websignin: '.var_export($response, true) );
                return Response::json($response,
                    200
                    );                                 
                               
            }
            else{
                //email no existe en la BBDD.
                if(!isset($name) || !isset($last_name)){
                    $error_code = 103;
                    return Response::json(array(
                        'success' => false,
                        'error_code' => $error_code,
                        'error_message' => $array_errors[$error_code]),
                        200
                        );
                }
                else{
                    //Registrar al usuario
                                         
                }
                
            }    

        }
        else{
            //there are not email in the token
            $error_code = 601;
            return Response::json(array(
                'success' => false,
                'error_code' => $error_code,
                'error_message' => $array_errors[$error_code]),
                200
                );

        }
    }
 
    public function checkLogin()
    {
        $array_errors = Config::get('ws_errors');

        Validator::extend('pass', function($attribute, $value)
        {
            return preg_match('/^([a-zA-Z0-9_@#$%&*]{6,20})$/', $value);
        });

        $rules = array(
            'email' => 'required|email|exists:users,user_email',
            'pass' => 'required|min:6|pass',
            'reg_id' => 'required',
        );
 
        $validator = Validator::make(Input::all(), $rules);
     
        $array_messages_error = $validator->messages()->getMessages();
        
        $error_message = array();   
        $error_code = 666;
//var_dump($array_messages_error);
        if ($validator->fails()){
            foreach ($array_messages_error as $key => $value) {
                $error_message[$key] = $value[0];
                $needle = 'field is required';
                $pos = strripos($value[0], $needle);
                //falta algún campo requerido
                if($pos !== false){
                    $error_code = 300;
                    break;
                }
                else{
                    $needle = 'valid email address';
                    $pos = strripos($value[0], $needle);
                    //el formato del email no es el correcto
                    if($pos !== false){
                        $error_code = 106;
                        break;
                    }
                    else{
                        $needle = 'validation.pass';
                        $pos1 = strripos($value[0], $needle);
                        $needle = 'at least 6 characters';
                        $pos2 = strripos($value[0], $needle);
                        //el formato de la contraseña no es correcto
                        if($pos1 !== false || $pos2 !== false){
                            $error_code = 104;  
                            break;                  
                        }
                        else{                            
                            $needle = 'The selected email is invalid';
                            $pos = strripos($value[0], $needle);
                            //el email no se encuentra en BBDD
                            if($pos !== false){
                                $error_code = 104;  
                                break;                  
                            }
                        }
                    }
                }                
                
            }
            return Response::json(array(
                'success' => false,
                'error_code' => $error_code,
                'error_message' => $array_errors[$error_code]),
                200
                );
         }

        $email = Input::get('email', NULL);
        $pass = Input::get('pass', NULL);
        $gcm_reg_id = Input::get('reg_id', NULL);

        $user = User::where(array('user_email'=>$email))->first();
                         
        if(isset($user) && $user != false && !empty($user)){
            $hashedPassword = $user->user_pass;
            if (!Hash::check($pass, $hashedPassword)){
                //The password entered is incorrect
                $error_code = 104;
            }
            elseif(!$user->user_confirm){
                //el email no se ha confirmado
                $error_code = 105;
            }
            else{
                $user_id = $user->user_id;
                $return = GoogleCloud::save($user_id, $gcm_reg_id);
                if($return){
                    return Response::json(array(
                        'success' => true,
                        'user_id' => $user_id,
                        'user_name'=> $user->user_nm,
                        'user_last_name'=>$user->user_last_nm),
                        200
                        );
                }
                
            }
            
        }
        else{
            //email no existe en la BBDD
            $error_code = 103;
        }    
        return Response::json(array(
            'success' => false,
            'error_code' => $error_code,
            'error_message' => $array_errors[$error_code]),
            200
            );
    }

    public function logOut(){
        $array_errors = Config::get('ws_errors');
       
        //define parameters validator
        $rules = array(
            'user_id' => 'required|integer|exists:users,user_id,deleted_at,NULL',
            'reg_id' => 'required',
        );         
        $messages = array(
            'required' => 'The :attribute field is required.',
            'exists' => 'The :attribute not exist.',
            'integer' => 'The :attribute must be a integer',
        );
        // doing the validation, passing post data and rules
        $validator = Validator::make(Input::all(), $rules, $messages);  
     
        $array_messages_error = $validator->messages()->getMessages();
        
        $error_message = array();   
        $error_code = 666;
        if ($validator->fails()){
            foreach ($array_messages_error as $key => $value) {
                $error_message[$key] = $value[0];
                $needle = 'field is required';
                $pos = strripos($value[0], $needle);
                //falta algún campo requerido
                if($pos !== false){
                    $error_code = 300;
                    break;
                }
                else{
                    $needle = 'not exist';
                    $pos = strripos($value[0], $needle);
                    if($pos !== false){
                        $error_code = 108;
                        break;
                    }
                    else{
                        $needle = 'must be a integer';
                        $pos = strripos($value[0], $needle);
                        if($pos !== false){
                            $error_code = 107;
                            break;
                        }
                    }
                }  
            }
            return Response::json(array(
                'success' => false,
                'error_code' => $error_code,
                'error_message' => $array_errors[$error_code]),
                200
                );
        }
        //end validation
        //capture the variables
        $user_id = Input::get('user_id', NULL);
        $gcm_reg_id = Input::get('reg_id', NULL);

        $gcm_registers = Gcmregister::where(array('user_id' => $user_id, 'gcm_reg_id'=> $gcm_reg_id))->get();

        foreach ($gcm_registers as $gcm_register) {
            if(isset($gcm_register) && !empty($gcm_register) && $gcm_register != false){
                $gcm_register->delete();            
            }
        }
        
        return Response::json(array(
        'success' => true,),
        200
        );
    } 

    public function addUser()
    {
        $array_errors = Config::get('ws_errors');

        Validator::extend('pass', function($attribute, $value)
        {
            return preg_match('/^([a-zA-Z0-9_@#$%&*]{6,20})$/', $value);
        });
        
        $rules = array(
            'email' => 'required|email|unique:users,user_email',
            'pass' => 'required|min:6|pass',
            'name' => 'required',
        );
 
        // doing the validation, passing post data and rules
        $validator = Validator::make(Input::all(), $rules);
     
        $array_messages_error = $validator->messages()->getMessages();
        
        $error_message = array();   
        $error_code = 666;
        if ($validator->fails()){
            foreach ($array_messages_error as $key => $value) {
                $error_message[$key] = $value[0];
                $needle = 'field is required';
                $pos = strripos($value[0], $needle);
                //falta algún campo requerido
                if($pos !== false){
                    $error_code = 300;
                    break;
                }
                else{
                    $needle = 'valid email address';
                    $pos = strripos($value[0], $needle);
                    //el formato del email no es el correcto
                    if($pos !== false){
                        $error_code = 106;
                        break;
                    }
                    else{
                        $needle = 'validation.pass';
                        $pos1 = strripos($value[0], $needle);
                        $needle = 'at least 6 characters';
                        $pos2 = strripos($value[0], $needle);
                        //el formato de la contraseña no es correcto -> no se encuentra en BBDD
                        if($pos1 !== false || $pos2 !== false){
                            $error_code = 104;  
                            break;                  
                        }
                        else{
                            $needle = 'The email has already been taken';
                            $pos = strripos($value[0], $needle);
                            //el email ya existe en la BBDD
                            if($pos !== false){
                                $error_code = 102;
                                break;
                            }
                        }
                    }
                }  
            }
            return Response::json(array(
                'success' => false,
                'error_code' => $error_code,
                'error_message' => $array_errors[$error_code]),
                200
                );
         }


        $email = Input::get('email', NULL);
        $pass = Input::get('pass', NULL);
        $name = Input::get('name', NULL);
        $lastname = Input::get('last_name', NULL);;

        $user = array(
            'email'=>$email,
            'name'=>$name,
        );

        // the data that will be passed into the mail view blade template
        $data = array(
            'detail'=>'http://digger.com/dev/public/index.php/verify_email?email='.$email,
            'name'  => $user['name'],
        );
        Mail::queueOn('verify_email','emails.welcome', $data, function($message) use ($user)
        {
            $message->to($user['email'], $user['name'])->subject('Confirm email');
            
        });

        //$bad_email = Mail::failures();
        //var_dump($bad_email);
        $user = new User;
        $user->user_email = $email;
        $hashedPassword = Hash::make($pass);
        $user->user_pass = $hashedPassword;
        $user->user_nm = $name;
        $user->user_last_nm = $lastname;
        $user->save();

        return Response::json(array(
            'success' => true),
            200
            );            
    }

    public function addUserGPlus()
    {

        $email = Input::get('email', NULL);
        $name = Input::get('name', NULL);
        $lastname = Input::get('last_name', NULL);;
        $gcm_reg_id = Input::get('reg_id', NULL);
        $array_errors = Config::get('ws_errors');

        $rules = array(
            'email' => 'required|email|unique:users,user_email',
            'name' => 'required',
            'last_name' => 'required',
            'reg_id' => 'required',
        );
 
        // doing the validation, passing post data and rules
        $validator = Validator::make(Input::all(), $rules);
     
        $array_messages_error = $validator->messages()->getMessages();
        
        $error_message = array();   
        $error_code = 666;
        if ($validator->fails()){
            foreach ($array_messages_error as $key => $value) {
                $error_message[$key] = $value[0];
                $needle = 'field is required';
                $pos = strripos($value[0], $needle);
                //falta algún campo requerido
                if($pos !== false){
                    $error_code = 300;
                    break;
                }
                else{
                    $needle = 'valid email address';
                    $pos = strripos($value[0], $needle);
                    //el formato del email no es el correcto
                    if($pos !== false){
                        $error_code = 106;
                        break;
                    }
                    else{
                        $needle = 'validation.pass';
                        $pos1 = strripos($value[0], $needle);
                        $needle = 'at least 6 characters';
                        $pos2 = strripos($value[0], $needle);
                        //el formato de la contraseña no es correcto -> no se encuentra en BBDD
                        if($pos1 !== false || $pos2 !== false){
                            $error_code = 104;  
                            break;                  
                        }
                        else{
                            $needle = 'The email has already been taken';
                            $pos = strripos($value[0], $needle);
                            //el email ya existe en la BBDD
                            if($pos !== false){
                                $user = User::findUserByEmail($email);
                                $user_id = $user->user_id;  
                                break;
                            }
                        }
                    }
                }  
            }
            if(!isset($user_id)){
                return Response::json(array(
                'success' => false,
                'error_code' => $error_code,
                'error_message' => $array_errors[$error_code]),
                200
                );
            }
            
         }     

        if(!isset($user_id)){
            $user = new User;
            $user->user_email = $email;       
            $user->user_nm = $name;
            $user->user_last_nm = $lastname;
            $user->save();
            $user_id = $user->user_id;
        }
        

        $return = GoogleCloud::save($user_id, $gcm_reg_id);

        return Response::json(array(
            'success' => true,
            'user_id'=> $user_id),
            200
            );            
    }

    public function verify_email()
    {
        $email = Input::get('email', NULL);

        $rules = array(
            'email' => 'required|email|exists:users,user_email',//que el validator compruebe si existe, que tenga que existir
        ); 
        
        $messages = array(
            'required' => 'The :attribute field is required.',
            'email' => 'The format of the :attribute is invalid.',
            'exists' => 'The :attribute entered is incorrect.',
        );   

        $validator = Validator::make(Input::all(), $rules, $messages);  
               
        $error_message = array();
        $info ="The email has been verified";

         if ($validator->fails()){
            $info = $validator->messages()->first('email');
            //mostrar una vista donde se especifique el error del validator
         }
         else{
            $user = User::where(array('user_email'=>$email))->first();
            if(isset($user) && !empty($user)){
                if($user->user_confirm == true){
                    $info = "The email entered is already verified";
                }
                else{
                    $user->user_confirm = true;
                    $success = $user->save();               
                    if($success){
                         //mostrar una vista donde se especifique que el email se ha verificado satisfactorimente
                        $info ="The email has been verified";
                    }
                    else{
                        $info = "Problems in the paradise";
                         //mostrar una vista donde se especifique que el email no se ha verificado satisfactorimente por errores internos
                    }
                }
                
            }

         }
         return View::make('verify_email')->with('info', $info);
    }

    public function setProfile()
    {
        $array_errors = Config::get('ws_errors');
       
        //define parameters validator

        Validator::extend('strings', function($attribute, $value)
        {
            return preg_match('/^(.*)$/', $value);
        });

        $rules = array(
            'user_id' => 'required|integer|exists:users,user_id,deleted_at,NULL',
            'user_name' => 'required|string',
            'user_last_name' => 'required|string',
            'user_country_ISO3' => 'strings|exists:countries,country_ISO3',
            'user_city' => 'string',
            'user_birth_date' => 'numeric',
            'user_sex' => 'in:Male,Female,Other',
        );

         
        $messages = array(
            'required' => 'The :attribute field is required.',
            'exists' => 'The :attribute not exist.',
            'numeric' => 'The :attribute must be a number',
            'string' => 'The :attribute must be a string',
            'in' => 'The :attribute must be the correct type',
        );   

        // doing the validation, passing post data and rules
        $validator = Validator::make(Input::all(), $rules, $messages);  
     
        $array_messages_error = $validator->messages()->getMessages();
        
        $error_message = array();   
        $error_code = 666;
        if ($validator->fails()){
            foreach ($array_messages_error as $key => $value) {
                $error_message[$key] = $value[0];
                $needle = 'field is required';
                $pos = strripos($value[0], $needle);
                //falta algún campo requerido
                if($pos !== false){
                    $error_code = 300;
                    break;
                }
                else{
                    $needle = 'not exist';
                    $pos = strripos($value[0], $needle);
                    if($pos !== false){
                        $error_code = 108;
                        break;
                    }
                    else{
                        $needle = 'must be a integer';
                        $pos = strripos($value[0], $needle);
                        if($pos !== false){
                            $error_code = 107;
                            break;
                        }
                        else{
                            $needle = 'must be a string';
                            $pos = strripos($value[0], $needle);
                            if($pos !== false){
                                $error_code = 107;
                                break;                                
                            }
                            else{
                                $needle = 'must be the correct type';
                                $pos = strripos($value[0], $needle);
                                if($pos !== false){
                                    $error_code = 113;
                                    break;
                                }                                
                            }
                        }
                    }
                }  
            }
            return Response::json(array(
                'success' => false,
                'error_code' => $error_code,
                'error_message' => $array_errors[$error_code].$value[0]),
                200
                );
        }
         //end validation

        //capture the variables

        $user_id = Input::get('user_id', NULL);
        $user_name = Input::get('user_name', NULL);
        $user_last_name = Input::get('user_last_name', NULL);
        $user_country_ISO3 = Input::get('user_country_ISO3', NULL);
        $user_city = Input::get('user_city', NULL);
        $user_birth_date = date('Y-m-d H:i:s', Input::get('user_birth_date', 0));
        $user_sex = Input::get('user_sex', NULL);
        $mul_content = Input::get('image_content', NULL);

         //edit the profile
        $user = User::find($user_id);
        $user->user_nm = $user_name;
        $user->user_last_nm = $user_last_name;

        if(isset($user_country_ISO3) && !empty($user_country_ISO3)){
            $country = Country::findCountryByISO3($user_country_ISO3);
            $user->country_id = $country->country_id;
        }
        
        if(isset($user_city) && !empty($user_city))
            $user->user_city = $user_city;     
        if(isset($user_sex) && !empty($user_sex))   
            $user->user_sex = $user_sex;
        if(isset($user_birth_date) && !empty($user_birth_date))
            $user->user_birth_date = $user_birth_date;
        $user->save();

        Contact::pushContact($user_id);

        return Response::json(array(
            'success' => true),
            200
            );    

    }   

    public function getProfile()
    {
        $array_errors = Config::get('ws_errors');
       
        //define parameters validator
        $rules = array(
            'user_id' => 'required|integer|exists:users,user_id,deleted_at,NULL',
        );         
        $messages = array(
            'required' => 'The :attribute field is required.',
            'exists' => 'The :attribute not exist.',
            'integer' => 'The :attribute must be a integer',
        );
        // doing the validation, passing post data and rules
        $validator = Validator::make(Input::all(), $rules, $messages);  
     
        $array_messages_error = $validator->messages()->getMessages();
        
        $error_message = array();   
        $error_code = 666;
        if ($validator->fails()){
            foreach ($array_messages_error as $key => $value) {
                $error_message[$key] = $value[0];
                $needle = 'field is required';
                $pos = strripos($value[0], $needle);
                //falta algún campo requerido
                if($pos !== false){
                    $error_code = 300;
                    break;
                }
                else{
                    $needle = 'not exist';
                    $pos = strripos($value[0], $needle);
                    if($pos !== false){
                        $error_code = 108;
                        break;
                    }
                    else{
                        $needle = 'must be a integer';
                        $pos = strripos($value[0], $needle);
                        if($pos !== false){
                            $error_code = 107;
                            break;
                        }
                    }
                }  
            }
            return Response::json(array(
                'success' => false,
                'error_code' => $error_code,
                'error_message' => $array_errors[$error_code]),
                200
                );
        }
        //end validation

        //capture the variables
        $user_id = Input::get('user_id', NULL);

        $user = User::find($user_id);
        $country = Country::find($user->country_id);

        $return = array(
            'success' => true,
            'user_id' => $user_id,
            'user_name' => $user->user_nm,
            'user_last_name' => $user->user_last_nm,
            'user_email' =>$user->user_email);

        
        if(isset($country) && !empty($country)){
            $ISO3 = $country->country_ISO3;
            if (isset($ISO3) && !empty($ISO3)) {
                $return['user_country_ISO3'] = $ISO3;
            }
        }
        
        $user_city= $user->user_city;
        if (isset($user_city) && !empty($user_city)) {
            $return['user_city'] = $user_city;
        }

        $user_sex= $user->user_sex;
        if (isset($user_sex) && !empty($user_sex)) {
            $return['user_sex'] = $user_sex;
        }

        $user_birth_date= isset($user->user_birth_date) ? strtotime($user->user_birth_date) : NULL;
        if (isset($user_birth_date) && !empty($user_birth_date)) {
            $return['user_birth_date'] = $user_birth_date;
        }

        $mult_id = $user->mult_id;
        if(isset($mult_id)){
            $multimedia = Multimedia::find($mult_id);
            $date_array = $multimedia->updated_at;
            $return["img_update_time"] = $date_array->timestamp;
        }

        return Response::json($return,
            200
            );

    }

    public function getPublicProfile()
    {
        $array_errors = Config::get('ws_errors');
       
        //define parameters validator
        $rules = array(
            'user_id' => 'required|integer|exists:users,user_id,deleted_at,NULL',
            'prof_user_id'  => 'required|integer|exists:users,user_id',            
        );         
        $messages = array(
            'required' => 'The :attribute field is required.',
            'exists' => 'The :attribute not exist.',
            'integer' => 'The :attribute must be a integer',
        );
        // doing the validation, passing post data and rules
        $validator = Validator::make(Input::all(), $rules, $messages);  
     
        $array_messages_error = $validator->messages()->getMessages();
        
        $error_message = array();   
        $error_code = 666;
        if ($validator->fails()){
            foreach ($array_messages_error as $key => $value) {
                $error_message[$key] = $value[0];
                $needle = 'field is required';
                $pos = strripos($value[0], $needle);
                //falta algún campo requerido
                if($pos !== false){
                    $error_code = 300;
                    break;
                }
                else{
                    $needle = 'not exist';
                    $pos = strripos($value[0], $needle);
                    if($pos !== false){
                        $error_code = 108;
                        break;
                    }
                    else{
                        $needle = 'must be a integer';
                        $pos = strripos($value[0], $needle);
                        if($pos !== false){
                            $error_code = 107;
                            break;
                        }
                    }
                }  
            }
            return Response::json(array(
                'success' => false,
                'error_code' => $error_code,
                'error_message' => $array_errors[$error_code]),
                200
                );
        }
        //end validation

        //capture the variables
        $request_user_id = Input::get('user_id', NULL);
        $prof_user_id = Input::get('prof_user_id', NULL);

        $array_contacts_role = Config::get('contacts_role');

        $user = User::find($prof_user_id);

        $contact = Contact::findContact($request_user_id, $prof_user_id);

        $contact_opos = Contact::findContact( $prof_user_id, $request_user_id);
        

        
        
        $contact_role = isset($contact->con_ro_id)?$contact->con_ro_id: NULL;

        $return = array(
            'success' => true,
            'user_name' => $user->user_nm,
            'user_last_name' => $user->user_last_nm,
            'contact_role' => isset($contact_role)? $contact_role : $array_contacts_role['NOT_CONTACT_ROLE']);




        if (!isset($contact_opos) || $contact_opos->con_ro_id != $array_contacts_role['I_BLOCKED_ROLE']) {
            if(isset($user->country_id) && !empty($user->country_id)) {
                $country = Country::find($user->country_id);
                if(isset($country) && !empty($country)) {
                    $ISO3 = $country->country_ISO3;
                    if (isset($ISO3) && !empty($ISO3)) {
                        $return['user_country_ISO3'] = $ISO3;
                    }
                }
            }      
        
            $user_city= $user->user_city;
            if (isset($user_city) && !empty($user_city)) {
                $return['user_city'] = $user_city;
            }

            $mult_id = $user->mult_id;
            if(isset($mult_id)){
                $multimedia = Multimedia::find($mult_id);
                $date_array = $multimedia->updated_at;
                $return["img_update_time"] = $date_array->timestamp;
            }
            
        }        

        return Response::json($return,
            200
            );

    }

    public function saveGCMRegId(){
        $array_errors = Config::get('ws_errors');
       
        //define parameters validator
        $rules = array(
            'user_id' => 'required|integer|exists:users,user_id,deleted_at,NULL',
            'reg_id' => 'required',
        );         
        $messages = array(
            'required' => 'The :attribute field is required.',
            'exists' => 'The :attribute not exist.',
            'integer' => 'The :attribute must be a integer',
        );
        // doing the validation, passing post data and rules
        $validator = Validator::make(Input::all(), $rules, $messages);  
     
        $array_messages_error = $validator->messages()->getMessages();
        
        $error_message = array();   
        $error_code = 666;
        if ($validator->fails()){
            foreach ($array_messages_error as $key => $value) {
                $error_message[$key] = $value[0];
                $needle = 'field is required';
                $pos = strripos($value[0], $needle);
                //falta algún campo requerido
                if($pos !== false){
                    $error_code = 300;
                    break;
                }
                else{
                    $needle = 'not exist';
                    $pos = strripos($value[0], $needle);
                    if($pos !== false){
                        $error_code = 108;
                        break;
                    }
                    else{
                        $needle = 'must be a integer';
                        $pos = strripos($value[0], $needle);
                        if($pos !== false){
                            $error_code = 107;
                            break;
                        }
                    }
                }  
            }
            return Response::json(array(
                'success' => false,
                'error_code' => $error_code,
                'error_message' => $array_errors[$error_code]),
                200
                );
        }
        //end validation

        //capture the variables
        $user_id = Input::get('user_id', NULL);
        $gcm_reg_id = Input::get('reg_id', NULL);

        $return = GoogleCloud::save($user_id, $gcm_reg_id);

        return Response::json(array(
        'success' => true,),
        200
        );
    } 


    public function searchUsers(){
        $array_errors = Config::get('ws_errors');
        $array_contacts_role = Config::get('contacts_role');
       
        //define parameters validator
        $rules = array(
            'user_id' => 'required|integer|exists:users,user_id,deleted_at,NULL',
            'query_text' => 'required',
            'amount_users' => 'required|numeric|min:1',
        );         
        $messages = array(
            'required' => 'The :attribute field is required.',
            'exists' => 'The :attribute not exist.',
            'integer' => 'The :attribute must be a integer',
            'numeric' => 'The :attribute must be a number',
            'min' => 'The :attribute must be higher than 0',
        );
        // doing the validation, passing post data and rules
        $validator = Validator::make(Input::all(), $rules, $messages);  
     
        $array_messages_error = $validator->messages()->getMessages();
        
        $error_message = array();   
        $error_code = 666;
        if ($validator->fails()){
            foreach ($array_messages_error as $key => $value) {
                $error_message[$key] = $value[0];
                $needle = 'field is required';
                $pos = strripos($value[0], $needle);
                //falta algún campo requerido
                if($pos !== false){
                    $error_code = 300;
                    break;
                }
                else{
                    $needle = 'not exist';
                    $pos = strripos($value[0], $needle);
                    if($pos !== false){
                        $error_code = 108;
                        break;
                    }
                    else{
                        $needle = 'must be a integer';
                        $pos = strripos($value[0], $needle);
                        if($pos !== false){
                            $error_code = 107;
                            break;
                        }
                    }
                }  
            }
            return Response::json(array(
                'success' => false,
                'error_code' => $error_code,
                'error_message' => $array_errors[$error_code]),
                200
                );
        }
        //end validation

        //capture the variables
        $user_id = Input::get('user_id', NULL);
        $query_text = Input::get('query_text', NULL);
        $amount_users = Input::get('amount_users', NULL);

        $blocked_role = $array_contacts_role['I_BLOCKED_ROLE'];
        
        /*$users_object = DB::select( DB::raw("SELECT users.user_id, users.user_nm, users.user_last_nm, contacts.con_ro_id 
             FROM users 
             LEFT JOIN contacts
                ON users.user_id = contacts.contact_secondary_user_id 
                WHERE users.user_id != $user_id
                and contacts.con_ro_id != $blocked_role
                and not exists (select * from contacts where contacts.con_ro_id = $blocked_role and users.user_id = contacts.contact_secondary_user_id)
                and (users.user_nm LIKE '$query_text%' or users.user_nm LIKE '% $query_text%' or 
                users.user_last_nm LIKE '$query_text%' or users.user_last_nm LIKE '% $query_text%')                
                GROUP BY users.user_id LIMIT $amount_users"));*/

        /*$users_object = DB::select( DB::raw("SELECT users.user_id, users.user_nm, users.user_last_nm
             FROM users where users.user_id != $user_id and not exists (select * from contacts where contacts.con_ro_id = $blocked_role and users.user_id = contacts.contact_secondary_user_id)       
            and (users.user_nm LIKE '$query_text%' or users.user_nm LIKE '% $query_text%' or 
                users.user_last_nm LIKE '$query_text%' or users.user_last_nm LIKE '% $query_text%')                
                GROUP BY users.user_id LIMIT $amount_users"));*/

        $users_object = DB::select( DB::raw("SELECT users.user_id, users.user_nm, users.user_last_nm, contacts.con_ro_id 
             FROM users 
             LEFT JOIN contacts
                ON users.user_id = contacts.contact_secondary_user_id 
                WHERE users.user_id != $user_id                
                and (contacts.con_ro_id != $blocked_role or contacts.con_ro_id is NULL)
                and (users.user_nm LIKE '$query_text%' or users.user_nm LIKE '% $query_text%' or 
                users.user_last_nm LIKE '$query_text%' or users.user_last_nm LIKE '% $query_text%')                
                GROUP BY users.user_id LIMIT $amount_users"));



        



        $users_return = array();

        foreach ($users_object as $user_object) {
            $users_return[] = array("user_id"=>$user_object->user_id,"name"=>$user_object->user_nm,
                "last_name"=>$user_object->user_last_nm,"contact_role"=>$user_object->con_ro_id);
        }


        return Response::json(array(
        'success' => true,
        'users' => $users_return),
        200
        );
    }


    public function getContactList(){
        $array_errors = Config::get('ws_errors');
        $array_contacts_role = Config::get('contacts_role');        
       
        //define parameters validator
        $rules = array(
            'user_id' => 'required|integer|exists:users,user_id,deleted_at,NULL',
            'update_time' => 'numeric',
        );         
        $messages = array(
            'required' => 'The :attribute field is required.',
            'exists' => 'The :attribute not exist.',
            'integer' => 'The :attribute must be a integer',
            'numeric' => 'The :attribute must be a number',
        );
        // doing the validation, passing post data and rules
        $validator = Validator::make(Input::all(), $rules, $messages);  
     
        $array_messages_error = $validator->messages()->getMessages();
        
        $error_message = array();   
        $error_code = 666;
        if ($validator->fails()){
            foreach ($array_messages_error as $key => $value) {
                $error_message[$key] = $value[0];
                $needle = 'field is required';
                $pos = strripos($value[0], $needle);
                //falta algún campo requerido
                if($pos !== false){
                    $error_code = 300;
                    break;
                }
                else{
                    $needle = 'not exist';//expected error
                    $pos = strripos($value[0], $needle);
                    if($pos !== false){
                        $error_code = 108;
                        break;
                    }
                    else{
                        $needle = 'must be a integer';
                        $pos = strripos($value[0], $needle);
                        if($pos !== false){
                            $error_code = 107;
                            break;
                        }
                        else{
                            $needle = 'must be a number';
                            $pos = strripos($value[0], $needle);
                            if($pos !== false){
                                $error_code = 107;
                                break;
                            }
                            else{
                            $needle = 'must be higher than 0';
                            $pos = strripos($value[0], $needle);
                            if($pos !== false){
                                $error_code = 107;
                                break;
                            }                            
                        }                          
                        }   
                    }
                }  
            }
            return Response::json(array(
                'success' => false,
                'error_code' => $error_code,
                'error_message' => $array_errors[$error_code]),
                200
                );
        }
        //end validation

        //capture the variables
        $user_id = Input::get('user_id', NULL);
        $update_time = Input::get('update_time', NULL);

         
        $blocked_role = $array_contacts_role['I_BLOCKED_ROLE'];

        if(isset($update_time)){
            //transform the timestamp to date format
            $update_timestamp = isset($update_time) ? date('Y-m-d H:i:s', $update_time) : null;


            /*$contacts = DB::select( DB::raw("SELECT users.user_id, users.user_nm,users.user_last_nm, users.mult_id, contacts.con_ro_id, contacts.updated_at 
                FROM users LEFT JOIN contacts ON users.user_id = contacts.contact_secondary_user_id WHERE contacts.contact_primary_user_id = $user_id 
                AND not exists (select * from contacts where contacts.con_ro_id = $blocked_role and users.user_id = contacts.contact_primary_user_id)
                AND contacts.updated_at > '$update_timestamp' ORDER BY contacts.updated_at DESC"));   

            
        }
        else{
            $contacts = DB::select( DB::raw("SELECT users.user_id, users.user_nm,users.user_last_nm, users.mult_id, contacts.con_ro_id, contacts.updated_at 
                FROM users LEFT JOIN contacts ON users.user_id = contacts.contact_secondary_user_id WHERE contacts.contact_primary_user_id = $user_id 
                AND not exists (select * from contacts where contacts.con_ro_id = $blocked_role and users.user_id = contacts.contact_primary_user_id)
                ORDER BY contacts.updated_at DESC"));               

        }*/
        $contacts = DB::select( DB::raw("SELECT users.user_id, users.user_nm,users.user_last_nm, users.mult_id, contacts.con_ro_id, contacts.updated_at 
                FROM users LEFT JOIN contacts ON users.user_id = contacts.contact_secondary_user_id WHERE contacts.contact_primary_user_id = $user_id 
                AND contacts.updated_at > '$update_timestamp' ORDER BY contacts.updated_at DESC"));   

        }
        else{
            $contacts = DB::select( DB::raw("SELECT users.user_id, users.user_nm,users.user_last_nm, users.mult_id, contacts.con_ro_id, contacts.updated_at 
                FROM users LEFT JOIN contacts ON users.user_id = contacts.contact_secondary_user_id WHERE contacts.contact_primary_user_id = $user_id 
                ORDER BY contacts.updated_at DESC"));               

        }         
         
        if(count($contacts) == 0){
            return Response::json(array(
                'success' => true,
                'up_to_date' => true),
                200
                );
        }
        else{
            $contacts_return = array();
            foreach ($contacts as $contact) {
                $conten_contact = array();
                $updated_at = strtotime($contact->updated_at);
                $content_contact = array("user_id"=>$contact->user_id,"name"=>$contact->user_nm,
                    "contact_role"=>$contact->con_ro_id, "update_time"=>$updated_at);

                $user_last_nm = $contact->user_last_nm;
                if (isset($user_last_nm) && !empty($user_last_nm)) {
                    $content_contact['last_name'] = $user_last_nm;
                }


            $contact_opos = Contact::findContact($contact->user_id, $user_id);
            if (!isset($contact_opos) || $contact_opos->con_ro_id != $blocked_role) {

                    $mult_id = isset($contact->mult_id)? $contact->mult_id: NULL;
                    if(isset($mult_id)){
                        $multimedia = Multimedia::find($mult_id);
                        $date_array = $multimedia->updated_at;
                        $content_contact["img_update_time"] = $date_array->timestamp;
                    }     
                }
                $contacts_return[] = $content_contact;
            }


            return Response::json(array(
                'success' => true,
                'users' => $contacts_return,),
                200
                );
        }

    }

    public function setContactRole(){
        $array_errors = Config::get('ws_errors');
       
        //define parameters validator
        $rules = array(
            'user_id' => 'required|integer|exists:users,user_id,deleted_at,NULL',
            'contact_user_id' => 'required|integer|exists:users,user_id,deleted_at,NULL',
            'contact_role' => 'required|integer|exists:contact_roles,con_ro_id',
        );         
        $messages = array(
            'required' => 'The :attribute field is required.',
            'exists' => 'The :attribute not exist.',
            'integer' => 'The :attribute must be a integer',
        );
        // doing the validation, passing post data and rules
        $validator = Validator::make(Input::all(), $rules, $messages);  
     
        $array_messages_error = $validator->messages()->getMessages();
        
        $error_message = array();   
        $error_code = 666;
        if ($validator->fails()){
            foreach ($array_messages_error as $key => $value) {
                $error_message[$key] = $value[0];
                $needle = 'field is required';
                $pos = strripos($value[0], $needle);
                //falta algún campo requerido
                if($pos !== false){
                    $error_code = 300;
                    break;
                }
                else{
                    $needle = 'The contact_user_id not exist';
                    $pos = strripos($value[0], $needle);
                    if($pos !== false){
                        return Response::json(array(
                        'success' => true),
                        200
                        ); 
                    }
                    $needle = 'not exist';//expected error
                    $pos = strripos($value[0], $needle);
                    if($pos !== false){
                        $error_code = 108;
                        break;
                    }
                    else{
                        $needle = 'must be a integer';
                        $pos = strripos($value[0], $needle);
                        if($pos !== false){
                            $error_code = 107;
                            break;
                        }   
                    }
                }  
            }
            return Response::json(array(
                'success' => false,
                'error_code' => $error_code,
                'error_message' => $array_errors[$error_code]),
                200
                );
        }
        //end validation

        //capture the variables
        $origin_user_id = Input::get('user_id', NULL);
        $destiny_user_id = Input::get('contact_user_id', NULL);
        $contact_role_ask = Input::get('contact_role', NULL);

        $array_contacts_role = Config::get('contacts_role');

        $origin_contact = Contact::findContact($origin_user_id, $destiny_user_id);//user that ask for the change of role
        $destiny_contact = Contact::findContact($destiny_user_id, $origin_user_id);//user requested for the change of role
        
        //if there are a previous relation of both contacts
        if (isset($origin_contact)){// && isset($destiny_contact)){
            $contact_role_user_origin = $origin_contact->con_ro_id;  

                switch ($contact_role_user_origin) {
                    case $array_contacts_role['IS_CONTACT_ROLE']:

                        if ($contact_role_ask === $array_contacts_role['NOT_CONTACT_ROLE']) {

                                $origin_contact->con_ro_id = $array_contacts_role['NOT_CONTACT_ROLE'];
                                //$destiny_contact->con_ro_id = $array_contacts_role['NOT_CONTACT_ROLE'];
                                //Functions::complexDeleteDecisions(array($origin_user_id,$destiny_user_id));
                        }
                        if ($contact_role_ask === $array_contacts_role['I_BLOCKED_ROLE']) {

                            $origin_contact->con_ro_id = $array_contacts_role['I_BLOCKED_ROLE'];
                            //$destiny_contact->con_ro_id = $array_contacts_role['BEEN_BLOCKED_ROLE'];
                            //Functions::complexDeleteDecisions(array($origin_user_id,$destiny_user_id));
                        }
                        break; 
                    case $array_contacts_role['NOT_CONTACT_ROLE']:
                        if ($contact_role_ask === $array_contacts_role['I_BLOCKED_ROLE']) {

                                $origin_contact->con_ro_id = $array_contacts_role['I_BLOCKED_ROLE'];
                                //$destiny_contact->con_ro_id = $array_contacts_role['BEEN_BLOCKED_ROLE'];
                                //Functions::complexDeleteDecisions(array($origin_user_id,$destiny_user_id));
                        }
                        if ($contact_role_ask === $array_contacts_role['IS_CONTACT_ROLE']) {

                                $origin_contact->con_ro_id = $array_contacts_role['IS_CONTACT_ROLE'];
                        }

                        break;
                    case $array_contacts_role['I_BLOCKED_ROLE']:

                        if ($contact_role_ask === $array_contacts_role['NOT_CONTACT_ROLE']) {

                                $origin_contact->con_ro_id = $array_contacts_role['NOT_CONTACT_ROLE'];
                                //$destiny_contact->con_ro_id = $array_contacts_role['NOT_CONTACT_ROLE'];
                                //Functions::complexDeleteDecisions(array($origin_user_id,$destiny_user_id));
                        }
                        if ($contact_role_ask === $array_contacts_role['IS_CONTACT_ROLE']) {

                                $origin_contact->con_ro_id = $array_contacts_role['IS_CONTACT_ROLE'];
                        }
                        break;                   
                    default:                     
                        break;
            } 
            $origin_contact->updateContact();
            //$destiny_contact->updateContact();
        }
        else{//the first time in have contact
            $origin_contact = new Contact();
            //$destiny_contact = new Contact();

            $origin_contact->contact_primary_user_id = $origin_user_id;
            $origin_contact->contact_secondary_user_id = $destiny_user_id;
            $origin_contact->con_ro_id = $contact_role_ask; 


            /*$destiny_contact->contact_primary_user_id = $destiny_user_id;
            $destiny_contact->contact_secondary_user_id = $origin_user_id;
            $destiny_contact->con_ro_id = $array_contacts_role['BEEN_REQUESTED_ROLE'];*/

            $origin_contact->save();
            //$destiny_contact->save();
        }  

        return Response::json(array(
                'success' => true),
                200
                );
    }

    public function forgotPassword(){
        $array_errors = Config::get('ws_errors');
        
        $rules = array(
            'email' => 'required|email|exists:users,user_email'
        );
 
        $validator = Validator::make(Input::all(), $rules);
     
        $array_messages_error = $validator->messages()->getMessages();
        
        $error_message = array();   
        $error_code = 666;
        if ($validator->fails()){
            foreach ($array_messages_error as $key => $value) {
                $error_message[$key] = $value[0];
                $needle = 'field is required';
                $pos = strripos($value[0], $needle);
                //falta algún campo requerido
                if($pos !== false){
                    $error_code = 300;
                    break;
                }
                else{
                    $needle = 'valid email address';
                    $pos = strripos($value[0], $needle);
                    //el formato del email no es el correcto
                    if($pos !== false){
                        $error_code = 106;
                        break;
                    }                    
                    else{                            
                        $needle = 'The selected email is invalid';
                        $pos = strripos($value[0], $needle);
                        //el email no se encuentra en BBDD
                        if($pos !== false){
                            $error_code = 104;  
                            break;                  
                        }
                    }
                }   
            }                   
                
        
            return Response::json(array(
                'success' => false,
                'error_code' => $error_code,
                'error_message' => $array_errors[$error_code]),
                200
                );
        }

        $email = Input::get('email', NULL);

        $user = User::where(array('user_email'=>$email))->first();

        $name = $user->user_nm;

        $user = array(
            'email'=>$email,
            'name'=>$name,
        );

        // the data that will be passed into the mail view blade template
        $data = array(
            'detail'=>'http://digger.com/dev/public/index.php/generate_password?email='.$email,
            'name'  => $user['name'],
        );
        Mail::queueOn('forgot_password','emails.reset', $data, function($message) use ($user)
        {
            $message->to($user['email'], $user['name'])->subject('Reset password');
            
        });

        return Response::json(array(
            'success' => true),
            200
            ); 
    }


    public function generatePassword(){
        $array_errors = Config::get('ws_errors');
        
        $rules = array(
            'email' => 'required|email|exists:users,user_email'
        );
 
        $validator = Validator::make(Input::all(), $rules);
     
        $array_messages_error = $validator->messages()->getMessages();
        
        $error_message = array();   
        $error_code = 666;
        if ($validator->fails()){
            foreach ($array_messages_error as $key => $value) {
                $error_message[$key] = $value[0];
                $needle = 'field is required';
                $pos = strripos($value[0], $needle);
                //falta algún campo requerido
                if($pos !== false){
                    $error_code = 300;
                    break;
                }
                else{
                    $needle = 'valid email address';
                    $pos = strripos($value[0], $needle);
                    //el formato del email no es el correcto
                    if($pos !== false){
                        $error_code = 106;
                        break;
                    }                    
                    else{                            
                        $needle = 'The selected email is invalid';
                        $pos = strripos($value[0], $needle);
                        //el email no se encuentra en BBDD
                        if($pos !== false){
                            $error_code = 103;  
                            break;                  
                        }
                    }
                }   
            }                   
                
        
            return Response::json(array(
                'success' => false,
                'error_code' => $error_code,
                'error_message' => $array_errors[$error_code]),
                200
                );
        }

        $email = Input::get('email', NULL);

        $user = User::where(array('user_email'=>$email))->first();

        $pass = Functions::generatePass();
        $hashedPassword = Hash::make($pass);
        $name = $user->user_nm;
        $user->user_pass = $hashedPassword;
        $user->save();

        $user = array(
            'email'=>$email,
            'name'=>$name,
            'pass' =>$pass,
        );

        // the data that will be passed into the mail view blade template
        $data = array(
            'name'  => $user['name'],
            'pass'  => $user['pass'],
        );
        Mail::queueOn('generate_password','emails.generate', $data, function($message) use ($user)
        {
            $message->to($user['email'], $user['name'])->subject('New password');
            
        });

        $info = "Your new password have been sended to your email account";

        return View::make('generated_pass')->with('info', $info);
    }


public function changePassword()
    {
        $array_errors = Config::get('ws_errors');

        Validator::extend('pass', function($attribute, $value)
        {
            return preg_match('/^([a-zA-Z0-9_@#$%&*]{6,20})$/', $value);
        });

        $rules = array(
            'user_id' => 'required|integer|exists:users,user_id,deleted_at,NULL',
            'old_pass' => 'required|min:6|pass',
            'new_pass' => 'required|min:6|pass',
        );
 
        $validator = Validator::make(Input::all(), $rules);
     
        $array_messages_error = $validator->messages()->getMessages();
        
        $error_message = array();   
        $error_code = 666;
        if ($validator->fails()){
            foreach ($array_messages_error as $key => $value) {
                $error_message[$key] = $value[0];
                $needle = 'field is required';
                $pos = strripos($value[0], $needle);
                //falta algún campo requerido
                if($pos !== false){
                    $error_code = 300;
                    break;
                }
                else{
                    $needle = 'must be a integer';
                    $pos = strripos($value[0], $needle);
                    if($pos !== false){
                        $error_code = 107;
                        break;
                    }
                    else{
                        $needle = 'validation.pass';
                        $pos1 = strripos($value[0], $needle);
                        $needle = 'at least 6 characters';
                        $pos2 = strripos($value[0], $needle);
                        //el formato de la contraseña no es correcto
                        if($pos1 !== false || $pos2 !== false){
                            $error_code = 104;  
                            break;                  
                        }
                        else{                            
                            $needle = 'The selected email is invalid';
                            $pos = strripos($value[0], $needle);
                            //el email no se encuentra en BBDD
                            if($pos !== false){
                                $error_code = 104;  
                                break;                  
                            }
                        }
                    }
                }                
                
            }
            return Response::json(array(
                'success' => false,
                'error_code' => $error_code,
                'error_message' => $array_errors[$error_code]),
                200
                );
         }

        $user_id = Input::get('user_id', NULL);
        $old_pass = Input::get('old_pass', NULL);
        $new_pass = Input::get('new_pass', NULL);

        $user = User::find($user_id);

        $hashedPassword = $user->user_pass;
        if (Hash::check($old_pass, $hashedPassword)){
            $hashedPassword = Hash::make($new_pass);
            $user->user_pass = $hashedPassword;
            $user->save();
            return Response::json(array(
            'success' => true),
            200
            ); 
        }
        else{
            $error_code = 104;
            return Response::json(array(
                'success' => false,
                'error_code' => $error_code,
                'error_message' => $array_errors[$error_code]),
                200
                );
        }
    }




    public function deleteUser()
    {
        $array_errors = Config::get('ws_errors');

        $rules = array(
            'user_id' => 'required|integer|exists:users,user_id,deleted_at,NULL',
        );
 
        $validator = Validator::make(Input::all(), $rules);
     
        $array_messages_error = $validator->messages()->getMessages();
        
        $error_message = array();   
        $error_code = 666;
        if ($validator->fails()){
            foreach ($array_messages_error as $key => $value) {
                $error_message[$key] = $value[0];
                $needle = 'field is required';
                $pos = strripos($value[0], $needle);
                //falta algún campo requerido
                if($pos !== false){
                    $error_code = 300;
                    break;
                }
                else{
                    $needle = 'must be a integer';
                    $pos = strripos($value[0], $needle);
                    if($pos !== false){
                        $error_code = 107;
                        break;
                    }
                }                
                
            }
            return Response::json(array(
                'success' => false,
                'error_code' => $error_code,
                'error_message' => $array_errors[$error_code]),
                200
                );
         }

        $user_id = Input::get('user_id', NULL);
        $user = User::find($user_id);
        $user->delete();

        return Response::json(array(
            'success' => true),
            200
            ); 
    }


    public function NotifAck(){
        Log::info('Entra en NotifAck : ');
        $array_errors = Config::get('ws_errors');
        $array_participant_status = Config::get('participant_status');
       
        //define parameters validator
        $rules = array(
            'user_id' => 'required|integer|exists:users,user_id,deleted_at,NULL',
            'decision_id' => 'required|integer|exists:decisions,decision_id,deleted_at,NULL',
        );         
        $messages = array(
            'required' => 'The :attribute field is required.',
            'exists' => 'The :attribute not exist.',
            'integer' => 'The :attribute must be a integer',
        );
        // doing the validation, passing post data and rules
        $validator = Validator::make(Input::all(), $rules, $messages);  
     
        $array_messages_error = $validator->messages()->getMessages();
        
        $error_message = array();   
        $error_code = 666;
        if ($validator->fails()){
            foreach ($array_messages_error as $key => $value) {
                $error_message[$key] = $value[0];
                $needle = 'field is required';
                $pos = strripos($value[0], $needle);
                //falta algún campo requerido
                if($pos !== false){
                    $error_code = 300;
                    break;
                }
                else{
                    $needle = 'not exist';
                    $pos = strripos($value[0], $needle);
                    if($pos !== false){
                        $error_code = 108;
                        break;
                    }
                    else{
                        $needle = 'must be a integer';
                        $pos = strripos($value[0], $needle);
                        if($pos !== false){
                            $error_code = 107;
                            break;
                        }
                    }
                }  
            }
            return Response::json(array(
                'success' => false,
                'error_code' => $error_code,
                'error_message' => $array_errors[$error_code]),
                200
                );
        }
        //end validation

        //capture the variables
        $user_id = Input::get('user_id', NULL);
        $decision_id = Input::get('decision_id', NULL);

        $decision = Decision::find($decision_id);

        $participant = Participant::findParticipantByUserID_AND_DecID($user_id, $decision_id);
        //set status of participant to new
        Log::info('Participante:  '.var_export($participant, true));
        Log::info('Participante isset:  '.var_export(isset($participant), true));
        if(isset($participant)){
            Log::info('Participante status antes de status:  '.var_export($participant->par_stat_id, true));
            $status = intval($participant->par_stat_id);
             Log::info('Participante status:  '.var_export($status, true));
            if($status === $array_participant_status["NOT_RECEIVED"]){
                $participant->par_stat_id = isset($array_participant_status["NEW"])? $array_participant_status["NEW"]: NULL;
                $participant->updateParticipant();
                $update_time = $decision->pushDecision();

                $participants_object = Decision::getParticipants($decision_id);
                $participants_id = array();
                $array_regs_id = array();
                foreach ($participants_object as $participant){
                    if($participant->user_id != $user_id){
                       $array_temp_regs_id = Gcmregister::getRegIDsByUserId($participant->user_id);
                       $array_regs_id = array_merge($array_regs_id, $array_temp_regs_id);
                    }
                    $participants_id[] = $participant->user_id;
                }

                $data = array
                (
                    "notif_type" => "part_status_update",
                    "decision_id" => $decision_id,
                    "admin_id" => $decision->user_id,
                    "user_ids_participant" => $participants_id,
                    "user_id"=>$user_id,
                    "part_status" => $array_participant_status["NEW"],
                    "update_time" => $update_time,
                );
                
                Log::info('Tamaño total array de registros gcm  '.count($array_regs_id));
                if (count($array_regs_id)>0) {           
                    $return = GoogleCloud::send($array_regs_id, $data);
                 }

            }            
        } 

        return Response::json(array(
            'success' => true),
            200
            );

    }


}
?>
