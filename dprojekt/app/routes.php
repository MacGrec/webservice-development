<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('hello');
});


Route::post('login', array('uses' => 'UsersController@checkLogin'));

Route::post('logout', array('uses' => 'UsersController@logOut'));

Route::post('register', array('uses' => 'UsersController@addUser'));

Route::post('register_gplus', array('uses' => 'UsersController@addUserGPlus'));

Route::get('verify_email', array('uses' => 'UsersController@verify_email'));

Route::post('create_decision', array('uses' => 'DecisionsController@create'));

Route::post('fetch_decision', array('uses' => 'DecisionsController@getDecision')); //retrocompatibilidad

Route::post('delete_decision', array('uses' => 'DecisionsController@deleteDecision'));

Route::post('set_profile', array('uses' => 'UsersController@setProfile'));

Route::post('get_profile', array('uses' => 'UsersController@getProfile'));

Route::post('get_public_profile', array('uses' => 'UsersController@getPublicProfile'));

Route::post('save_gcm_reg_id', array('uses' => 'UsersController@saveGCMRegId'));

Route::post('update_decisions_list', array('uses' => 'DecisionsController@getDecisionList')); //retrocompatibilidad

Route::post('update_contacts_list', array('uses' => 'UsersController@getContactList'));

Route::post('search_users', array('uses' => 'UsersController@searchUsers'));

Route::post('update_contact_role', array('uses' => 'UsersController@setContactRole'));

Route::get('get_image', array('uses' => 'MultimediaController@getImage'));

Route::post('upload_image', array('uses' => 'MultimediaController@setImage'));

Route::post('send_preferences', array('uses' => 'DecisionsController@send_preference'));

Route::post('forgot_password', array('uses' => 'UsersController@forgotPassword'));

Route::get('generate_password', array('uses' => 'UsersController@generatePassword'));

Route::post('change_password', array('uses' => 'UsersController@changePassword'));

Route::post('update_decision_state', array('uses' => 'DecisionsController@updateDecision'));

Route::post('delete_user', array('uses' => 'UsersController@deleteUser'));

Route::post('signin', array('uses' => 'UsersController@SigIn'));

Route::post('web_signin', array('uses' => 'UsersController@WebSigIn'));

Route::post('feedback', array('uses' => 'AppsController@SaveFeedback'));

Route::any('info_feedback', array('uses' => 'AppsController@GetInfoFeedback'));

Route::any('dig', array('uses' => 'AppsController@RouterDig'));

Route::post('notif_ack', array('uses' => 'UsersController@NotifAck'));

Route::any('stats', array('uses' => 'InfoController@ShowStats'));

Route::post('join_decision', array('uses' => 'DecisionsController@AddParticipant'));





Route::post('delayed_response', array('uses' => 'DecisionsController@delayed'));

Route::get('testGCM', array('uses' => 'DecisionsController@test'));

Route::get('testhash', array('uses' => 'MultimediaController@testHash'));

Route::get('testCrypt', array('uses' => 'TestController@testCrypt'));

Route::get('testGoogleAPI', array('uses' => 'TestController@testGoogleAPI'));
?>